###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski
#
#  This file is part of nanoRVE.
#
#  nanoRVE is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  nanoRVE is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Choose CMake policies:
CMAKE_POLICY(SET CMP0048 NEW)   # Project command sets version variable.

# Require a modern version of CMake.
CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0 FATAL_ERROR)

# Define project name and version number.
PROJECT(atomicrex VERSION 0.1.0 LANGUAGES CXX C Fortran)

# Set default build type to "Release"
IF(NOT CMAKE_BUILD_TYPE) 
    SET(CMAKE_BUILD_TYPE Release)
ENDIF()

# Look in cmake subdirectory when searching for CMake modules.
LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# Check for compiler OpenMP support.
FIND_PACKAGE(OpenMP)

OPTION(USE_OPENMP "Use OpenMP parallelization" ${OPENMP_FOUND})

# Set up flags for Fortran compiler.
INCLUDE(cmake/FortranCompilerFlags.cmake)

# Enable C++11 standard.
IF(NOT CMAKE_VERSION VERSION_LESS "3.1")
	SET(CMAKE_CXX_STANDARD 11)
	SET(CMAKE_CXX_STANDARD_REQUIRED ON)
ELSE()
   	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
ENDIF()

# Compile code.
ADD_SUBDIRECTORY(src)
