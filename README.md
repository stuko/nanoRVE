# nanoRVE

**nanoRVE** is a simple molecular statics simulation code developed for an undisclosed purpose.

## Features

  * Supported potential type: EAM (embedded atom method)
  * Supported energy minimizer: L-BFGS-B
  * Structure file readers: POSCAR, LAMMPS dump
  * Periodic boundary conditions
  * Non-orthogonal simulation cells
  * Virial stress calculation

nanoRVE is written in C++11.

## Requirements and dependencies

  * C++11 compiler
  * Fortran compiler
  * [CMake](https://cmake.org)

## License

The **nanoRVE** code is made available under the GNU GPL v3 license.

## Contact

* [Alexander Stukowski](http://www.mawi.tu-darmstadt.de/mm/mm_mm_sw/gruppe_mm_sw_1/mitarbeiterdetails_mm_2307.en.jsp),
  Technische Universität Darmstadt, Germany