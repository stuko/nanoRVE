///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../nanoRVE.h"

namespace nanoRVE {

class AtomicStructure;	// Declared in AtomicStructure.h
class NeighborList;		// Declared in NeighborList.h

/**
* @brief      Base class for potential.
*
* @details    This class is used to store structure specific information
*             including atomic coordinates, neighbor lists, total energy,
*             volume, pressure etc.
*/
class Potential
{
public:

	/// Constructor.
	Potential(int numAtomTypes);

	/// Enables the interaction between two atom types.
	void enableInteraction(int speciesA, int speciesB) {
		assert(speciesA >= 0 && speciesA < _interactionFlags.size());
		assert(_interactionFlags[speciesA].size() == numAtomTypes());
		assert(speciesB >= 0 && speciesB < _interactionFlags[speciesA].size());
		_interactionFlags[speciesA][speciesB] = true;
		_interactionFlags[speciesB][speciesA] = true;
		_atomTypeFlags[speciesA] = true;
		_atomTypeFlags[speciesB] = true;
	}

	/// Returns whether the interaction between two atom types is enabled for this potential.
	bool interacting(int speciesA, int speciesB) const {
		assert(speciesA >= 0 && speciesA < _interactionFlags.size());
		assert(_interactionFlags[speciesA].size() == numAtomTypes());
		assert(speciesB >= 0 && speciesB < _interactionFlags[speciesA].size());
		return _interactionFlags[speciesA][speciesB];
	}

	/// Returns whether the given atom type is handled by this potential.
	/// If not, atoms of this species are completely ignored in the potential routine.
	bool isAtomTypeEnabled(int species) const {
		assert(species >= 0 && species < _atomTypeFlags.size());
		return _atomTypeFlags[species];
	}

	/// Returns the maximum cutoff of the potential.
	virtual double cutoff() const = 0;

	/// Computes the total energy and forces of the structure.
	virtual double computeEnergyAndForces(AtomicStructure& structure, NeighborList& neighborList) = 0;

	/// Computes the total energy of the structure.
	virtual double computeEnergy(AtomicStructure& structure, NeighborList& neighborList) = 0;

	/// Returns the number of bytes of working memory per atom required by the potential routine.
	/// The system will provide a memory buffer of the given size for each atomic structure that
	/// can be used by the potential routine.
	virtual size_t perAtomDataSize() const { return 0; }

	/// Returns the number of bytes of working memory per atom neighbor required by the potential routine.
	/// The system will provide a memory buffer of the given size for each atomic structure that
	/// can be used by the potential routine.
	virtual size_t perBondDataSize() const { return 0; }

	/// Returns the number of atom types 
	int numAtomTypes() const { return _atomTypeFlags.size(); }

public:

	/// This virtual method is called by the system before the potential is being used to compute energy and forces.
	/// Note that the method may be called several times.
	/// The default implementation does nothing.
	virtual void preparePotential() {}

private:

	/// This array controls which atom types are handled by this potential.
	/// Array indices are 0-based!
	std::vector<bool> _atomTypeFlags;

	/// This NxN table controls which interactions between atom types are described by this potential.
	/// Array indices are 0-based!
	std::vector<std::vector<bool>> _interactionFlags;
};

} // End of namespace
