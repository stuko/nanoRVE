///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../nanoRVE.h"

namespace nanoRVE {

/**
 * A one-dimensional cubic spline function.
 *
 * The spline consists of N knots with coordinates (x,y).
 *
 * Knots are positioned on a regular grid on the X axis, i.e.,
 * the X position of a knot is given by its index times the
 * grid spacing. The first knot is always at X=0.
 *
 * The first derivative of the function must be explicitly
 * specified at the first and the last knot.
 */
class GridCubicSpline
{
public:

	/// Initialization of the spline function.
	void init(int N, double delta, double derivStart = 0, double derivEnd = 0) {
		assert(N >= 2);
		assert(delta > 0);
		_N = N;
		_derivStart = derivStart;
		_derivEnd = derivEnd;
		_delta = delta;
		_xcutoff = delta * (N-1);
		Y.resize(N);
		Y2.resize(N);
	}

	/// Sets the Y value of the function at a knot.
	void setKnot(int i, double y) {
		assert(i >=0 && i < _N);
		Y[i] = y;
	}

	/// Returns the number of knots.
	int knotCount() const { return _N; }

	/// Returns first derivative of spline function at knot 0.
	double derivStart() const { return _derivStart; }

	/// Returns first derivative of spline function at the last knot.
	double derivEnd() const { return _derivEnd; }

	/// Returns the X coordinate of the i-th knot.
	double knotX(int i) const { assert(i >=0 && i < _N); return _delta * i; }

	/// Returns the Y coordinate of the i-th knot.
	double knotY(int i) const { assert(i >=0 && i < _N); return Y[i]; }

	/// Returns the second derivative at the i-th knot.
	double knotY2(int i) const { assert(i >=0 && i < _N); return Y2[i]; }

	/// Parses the spline knots from a file.
	bool parse(std::istream& stream);

	/// Calculates the second derivatives of the cubic spline.
	void prepareSpline();

	/// Evaluates the spline function at position x.
	double eval(double x) const;

	/// Evaluates the spline function and its first derivative at position x.
	double eval(double x, double& deriv) const;

	/// Returns the cutoff radius of this spline function.
	double cutoff() const { return _xcutoff; }

	/// Create a Gnuplot script that displays the spline function.
	void writeGnuplot(const std::string& filename, const std::string& title) const;

private:

	int _N = 0;					// Number of spline knots
	double _delta = 0;			// Distance between spline knots.
	double _xcutoff;			// The X coordinate of the last knot.
	std::vector<double> Y;		// Function values at spline knots
	std::vector<double> Y2;		// Second derivatives at spline knots
	double _derivStart;			// First derivative at knot 0
	double _derivEnd;			// First derivative at knot (N-1)
};

} // End of namespace
