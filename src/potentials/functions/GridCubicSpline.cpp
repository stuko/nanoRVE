///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "GridCubicSpline.h"

namespace nanoRVE {

using namespace std;

/******************************************************************************
 * Calculates the second derivatives of the cubic spline.
******************************************************************************/
void GridCubicSpline::prepareSpline()
{
	assert(knotCount() >= 2);

	vector<double> u(knotCount());
	Y2[0] = -0.5;
	u[0] = (3.0/_delta) * ((Y[1] - Y[0]) / _delta - derivStart());
	for(int i = 1; i <= knotCount()-2; i++) {
		const double sig = 0.5;
		double p = sig * Y2[i-1] + 2.0;
		Y2[i] = (sig - 1.0) / p;
		u[i] = (Y[i+1]-Y[i]) / _delta - (Y[i]-Y[i-1])/_delta;
		u[i] = (6.0 * u[i]/(_delta * 2.0) - sig*u[i-1])/p;
	}

	double qn = 0.5;
	double un = (3.0/_delta) * (derivEnd() - (Y[_N-1]-Y[_N-2])/_delta);
	Y2[_N-1] = (un - qn*u[_N-2]) / (qn * Y2[_N-2] + 1.0);
	for(int k = _N-2; k >= 0; k--) {
		Y2[k] = Y2[k] * Y2[k+1] + u[k];
	}
}

/******************************************************************************
 * Evaluates the spline function at position x.
******************************************************************************/
double GridCubicSpline::eval(double x) const
{
	assert(knotCount() >= 2);
	if(x > 0.0 && x < _xcutoff) {
		double k = x / _delta;
		int klo = (int)k;
		int khi = klo + 1;
		assert(klo >= 0 && khi < _N);
		// Do spline interpolation.
		double a = (double)khi - k;
		double b = k - (double)klo;
		return a * Y[klo] + b * Y[khi] + ((a*a*a - a) * Y2[klo] + (b*b*b - b) * Y2[khi])*(_delta*_delta)/6.0;
	}
	else if(x <= 0.0) {  // Left extrapolation.
		return Y.front() + derivStart() * x;
	}
	else {  // Right extrapolation.
		return Y.back() + derivEnd() * (x - _xcutoff);
	}
}

/******************************************************************************
 * Evaluates the spline function and its first derivative at position x.
******************************************************************************/
double GridCubicSpline::eval(double x, double& deriv) const
{
	assert(knotCount() >= 2);

	if(x > 0.0 && x < _xcutoff) {
		double k = x / _delta;
		int klo = (int)k;
		int khi = klo + 1;
		assert(klo >= 0 && khi < _N);
		// Do spline interpolation.
		double a = (double)khi - k;
		double b = k - (double)klo;
		deriv = (Y[khi] - Y[klo]) / _delta + ((3.0*b*b - 1.0) * Y2[khi] - (3.0*a*a - 1.0) * Y2[klo]) * _delta / 6.0;
		return a * Y[klo] + b * Y[khi] + ((a*a*a - a) * Y2[klo] + (b*b*b - b) * Y2[khi])*(_delta*_delta)/6.0;
	}
	else if(x <= 0.0) {  // Left extrapolation.
		deriv = derivStart();
		return Y.front() + derivStart() * x;
	}
	else {  // Right extrapolation.
		deriv = derivEnd();
		return Y.back() + derivEnd() * (x - _xcutoff);
	}
}

/******************************************************************************
 * Parses the spline knots from a file.
******************************************************************************/
bool GridCubicSpline::parse(std::istream& stream)
{
	assert(knotCount() >= 2);

	for(int i = 0; i < knotCount(); i++) {
		stream >> Y[i];
	}
	if(!stream)
		return false;

	prepareSpline();
	return true;
}

/******************************************************************************
 * Create a Gnuplot script that displays the spline function.
******************************************************************************/
void GridCubicSpline::writeGnuplot(const std::string& filename, const std::string& title) const
{
	ofstream stream(filename.c_str());
	if(!stream.is_open())
		throw runtime_error("Failed to open Gnuplot file for writing.");

	stream << "#!/usr/bin/env gnuplot" << endl;
	if(title.empty() == false) stream << "set title \"" << title << "\"" << endl;
	stream << "set xrange [0:" << cutoff() << "]" << endl;
	stream << "plot '-' with lines notitle, '-' with lines notitle" << endl;
	for(int i = 0; i < knotCount(); i++) {
		stream << knotX(i) << " " << knotY(i) << endl;
	}
	stream << "e" << endl;
	for(double x = 0; x <= cutoff(); x += _delta * 0.05) {
		double y = eval(x);
		stream << x << " " << y << endl;
	}
	stream << "e" << endl;
}

}
