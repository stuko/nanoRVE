///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Potential.h"

namespace nanoRVE {

using namespace std;


/******************************************************************************
* Constructor.
******************************************************************************/
Potential::Potential(int numAtomTypes)
{
	// Disable this potential for all atom types by default.
	// It needs to be explicitly enabled for certain types.
	_atomTypeFlags.resize(numAtomTypes, false);

	// Setup N x N interaction table.
	// Disable this potential for all type interactions by default.
	// It needs to be explicitly enabled for certain pairs of types.
	for(int i = 0; i < numAtomTypes; i++)
		_interactionFlags.push_back(vector<bool>(numAtomTypes));
}

};
