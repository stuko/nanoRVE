///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

/******************************************************************************
* STL Library
******************************************************************************/
#include <vector>
#include <list>
#include <map>
#include <set>
#include <limits>
#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <locale>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <string>
#include <cctype>
#include <iomanip>

/******************************************************************************
* Include a standard set of our own headers
******************************************************************************/
#include "util/FloatType.h"
#include "util/Logger.h"
#include "util/linalg/LinAlg.h"
