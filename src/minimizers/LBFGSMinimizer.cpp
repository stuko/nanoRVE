///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "LBFGSMinimizer.h"

/// The Fortran routine found in the file "lbfgsb.f"
extern "C" int setulb_(int *n, int *m, double *x, double *l, double *u,
                   int *nbd, double *f, double *g, double *factr,
                   double *pgtol, double *wa, int *iwa, char *task,
                   int *iprint, char *csave, int *lsave,
                   int *isave, double *dsave, int, int);

namespace nanoRVE {

using namespace std;

/******************************************************************************
* Initializes the minimizer.
******************************************************************************/
void LBFGSMinimizer::prepare(std::vector<double>&& x0,
		const std::function<double(const std::vector<double>&)>& func,
		const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient)
{
	Minimizer::prepare(std::move(x0), func, gradient);

	// Number of dimensions.
	int n = x0.size();
	assert(n >= 1);

	// Number of limited memory corrections.
	m = 10;

	if(pgtol <= 0.0)
		throw runtime_error("LBFGS Minimizer: Convergence threshold must be positive.");

#if 0
	iprint = 1; // We wish to have output at every iteration.
#else
	iprint = -1; // We wish to have no output.
#endif

	factr = 1e7;
	wa.resize((2*m + 4)*n + 12*m*m + 12*m);
	iwa.resize(3*n);
	g.resize(n);

	// We start the minimizer by setting task to START.
	csave[0] = '\0';
	strcpy(task, "START");
	for(int i = 5; i < 60; i++)
		task[i] = ' ';

	x = std::move(x0);
}

/******************************************************************************
* Sets constraints for the variation of the parameters.
******************************************************************************/
void LBFGSMinimizer::setConstraints(
		std::vector<BoundConstraints>&& constraintTypes,
		std::vector<double>&& lowerBounds,
		std::vector<double>&& upperBounds)
{
	_constraintTypes = std::move(constraintTypes);
	_lowerBounds = std::move(lowerBounds);
	_upperBounds = std::move(upperBounds);
}

/******************************************************************************
* Performs one minimization iteration.
******************************************************************************/
Minimizer::MinimizerResult LBFGSMinimizer::iterate()
{
	_itercount++;
	int n = x.size();

	assert(_constraintTypes.empty() || _constraintTypes.size() == n);
	assert(_lowerBounds.empty() || _lowerBounds.size() == n);
	assert(_upperBounds.empty() || _upperBounds.size() == n);
	_constraintTypes.resize(n, NO_BOUNDS);
	_lowerBounds.resize(n);
	_upperBounds.resize(n);

	setulb_(&n, &m, &x[0], &_lowerBounds[0], &_upperBounds[0], (int*)&_constraintTypes[0], &f, &g[0], &factr,
	                   &pgtol, &wa[0], &iwa[0], task, &iprint, csave, lsave, isave, dsave, 60, 60);

	if(strncmp(task, "FG", 2) == 0) {
		// The minimization routine has returned to request the
		// function f and gradient g values at the current x.
		evaluate();
		return MINIMIZATION_CONTINUE;
	}
	else if(strncmp(task, "NEW_X", 5) == 0) {
		// The minimization routine has returned with a new iterate,
		// and we have opted to continue the iteration (do nothing here).
		return MINIMIZATION_CONTINUE;
	}
	else if(strncmp(task, "CONV", 4) == 0) {
		return MINIMIZATION_CONVERGED;
	}
	else if(strncmp(task, "ABNO", 4) == 0) {
		return MINIMIZATION_ABNORMAL;
	}
	else {
		return MINIMIZATION_ERROR;
	}
}

/******************************************************************************
* Evaluates the function at the current x and computes the gradient.
******************************************************************************/
void LBFGSMinimizer::evaluate()
{
	assert(_func);
	if(_gradientFunc)
		f = _gradientFunc(x, g);
	else
		f = numericGradient(x, g);

	_gradNorm2 = 0;
	for(vector<double>::const_iterator g_iter = g.begin(); g_iter != g.end(); ++g_iter)
		_gradNorm2 += square(*g_iter);
	_gradNorm2 = sqrt(_gradNorm2);
}

}
