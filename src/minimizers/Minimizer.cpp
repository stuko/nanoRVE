///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Minimizer.h"
#include "LBFGSMinimizer.h"

namespace nanoRVE {

using namespace std;

/******************************************************************************
* Computes the gradient of the objective function using finite differences.
******************************************************************************/
double Minimizer::numericGradient(const vector<double>& x, vector<double>& g)
{
	assert(_func);

    vector<double>& nonconst_x = const_cast<vector<double>&>(x);

	vector<double>::iterator g_iter = g.begin();
	vector<double>::iterator x_iter = nonconst_x.begin();
	for(; x_iter != nonconst_x.end(); ++x_iter, ++g_iter) {
		double oldx = *x_iter;
		*x_iter = oldx + _gradientEpsilon;
		double e_plus = _func(nonconst_x);
		*x_iter = oldx - _gradientEpsilon;
		double e_minus = _func(nonconst_x);
		*x_iter = oldx;
		*g_iter = (e_plus - e_minus) / (2.0 * _gradientEpsilon);
	}

	return _func(x);
}

/******************************************************************************
* Initializes the minimizer.
* Must be called once before entering the minimization loop.
******************************************************************************/
void Minimizer::prepare(
		std::vector<double>&& x0,
		const std::function<double(const std::vector<double>&)>& func,
		const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient)
{
	_func = func;
	_gradientFunc = gradient;
	f = 0.0;
	_gradNorm2 = 0.0;
	_itercount = 0;
}

}
