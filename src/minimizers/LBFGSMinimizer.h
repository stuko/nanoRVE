///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../nanoRVE.h"
#include "Minimizer.h"

namespace nanoRVE {

class LBFGSMinimizer : public Minimizer
{
public:

	/// Initializes the minimizer by setting the objective function and the starting vector.
	/// Must be called once before entering the minimization loop.
	///
	/// \param x0 An R-value reference to the starting vector.
	///           The length of this vector determines the number of dimensions.
	///           The function transfers the vector to internal storage. That means the
	///           passed-in vector will no longer be valid after the function returns.
	/// \param func The object that computes the value of the objective function at a given point x.
	/// \param gradient An optional function object that computes the (analytic) gradient of the objective function
	///                 at a given point x (and also the value of the objective function).
	///                 If no gradient function is provided, and the minimization algorithm requires
	///                 the gradient, the minimizer will compute it using finite differences by
	///                 evaluating \a func several times.
	virtual void prepare(
			std::vector<double>&& x0,
			const std::function<double(const std::vector<double>&)>& func,
			const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient = std::function<double(const std::vector<double>&, std::vector<double>&)>()) override;

	/// Sets the constraints for variations of the parameters.
	/// Must be called after prepare() and before the minimization loop is entered.
	virtual void setConstraints(
			std::vector<BoundConstraints>&& constraintTypes,
			std::vector<double>&& lowerBounds,
			std::vector<double>&& upperBounds) override;

	/// Performs one minimization iteration.
	virtual MinimizerResult iterate() override;

private:

	/// Evaluates the function at the current x and computes the gradient.
	void evaluate();

	std::vector<BoundConstraints> _constraintTypes;
	std::vector<double> _lowerBounds;
	std::vector<double> _upperBounds;

	/// The current approximation to the solution.
	std::vector<double> x;

	/// The gradient of the objective function.
	std::vector<double> g;

	// Variables passed to the L-BFGS-B Fortran routine:
	char task[60];
	char csave[60];
	int lsave[4];
	double dsave[29];
	int isave[44];
	double factr;
	std::vector<int> iwa;
	int m;
	double pgtol = 1e-5;
	std::vector<double> wa;
	int iprint;
};

} // End of namespace
