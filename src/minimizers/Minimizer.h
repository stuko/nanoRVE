///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../nanoRVE.h"

namespace nanoRVE {

/**
  @brief Abstract base class for minimization algorithms.
 */
class Minimizer
{
public:

  // This enumeration indicates the result of a minimization step.
	enum MinimizerResult {
	  // The minimization continues.
		MINIMIZATION_CONTINUE,
		// The minimization has converged.
		MINIMIZATION_CONVERGED,
		// The minimization step has returned an abnormal result.
		MINIMIZATION_ABNORMAL,
		// An error was encountered during minimization.
		MINIMIZATION_ERROR,
	};

	// This enumeration indicates the treatment of bounds on parameters during the minimization
	enum BoundConstraints {
	  // No bounds on parameters.
		NO_BOUNDS = 0,
		// Lower bounds are enforced.
		LOWER_BOUNDS = 1,
		// Both lower and upper bounds are enforced.
		BOTH_BOUNDS = 2,
		// Upper bounds are enforced.
		UPPER_BOUNDS = 3
	};

public:

	/**
	   @brief Initializes the minimizer by setting the objective function and the starting vector.
	   @details Must be called once before entering the minimization loop.

	   @param x0 An R-value reference to the starting vector.
	   The length of this vector determines the number of dimensions.
	   The function transfers the vector to internal storage. That means the
	   passed-in vector will no longer be valid after the function returns.
	   @param func The object that computes the value of the objective function at a given point x.
	   @param gradient An optional function object that computes the (analytic) gradient of the objective function
	   at a given point x (and also the value of the objective function).
	   If no gradient function is provided, and the minimization algorithm requires
	   the gradient, the minimizer will compute it using finite differences by
	   evaluating \a func several times.
	*/
	virtual void prepare(
			std::vector<double>&& x0,
			const std::function<double(const std::vector<double>&)>& func,
			const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient = std::function<double(const std::vector<double>&, std::vector<double>&)>());

	/**
	   @brief Sets constraints for variations of the parameters.
	   @details Must be called after prepare() and before the minimization loop is entered.
	   All arguments passed to this function will be transferred into internal storage of this class.
	   The input vectors become invalid when the function returns.
	*/
	virtual void setConstraints(
			std::vector<BoundConstraints>&& constraintTypes,
			std::vector<double>&& lowerBounds,
			std::vector<double>&& upperBounds) {}

	/// Performs one minimization iteration.
	virtual MinimizerResult iterate() = 0;

	/// Returns the number of iterations performed so far.
	int itercount() const { return _itercount; }

	/// Returns the value of the objective function at the current x.
	double value() const { return f; }

	/// Returns the 2-norm of the gradient of the objective function at the current x.
	double gradientNorm2() const { return _gradNorm2; }

	/// Returns the limit for which the minimizer converges.
	double convergenceThreshold() const { return _convergenceThreshold; }

	/// Returns the maximum number of iterations for the minimizer.
	int maximumNumberOfIterations() const { return _maximumNumberOfIterations; }

	/// Set the maximum number of iterations for the minimizer.
	void setMaximumNumberOfIterations(int num) { _maximumNumberOfIterations = num; }

protected:

	/// Computes the gradient of the objective function using finite differences.
	double numericGradient(const std::vector<double>& x, std::vector<double>& g);

protected:

	/// Calculates the value of the objective function to be minimized.
	std::function<double(const std::vector<double>&)> _func;

	/// Calculates the gradient of the objective function.
	std::function<double(const std::vector<double>&, std::vector<double>&)> _gradientFunc;

	/// The value of the objective function;
	double f = 0.0;

	/// The 2-norm of the gradient.
	double _gradNorm2 = 0.0;

	/// Number of iterations performed so far.
	int _itercount = 0;

	/// The limit for which the minimizer converges.
	double _convergenceThreshold = 1e-5;

	/// Maximum number of iterations for the minimizer.
	int _maximumNumberOfIterations = 100;

	// The epsilon parameter for numeric calculation of gradient using finite differences.
	double _gradientEpsilon = 1e-6;
};

} // End of namespace
