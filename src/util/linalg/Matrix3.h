///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <array>
#include <ostream>
#include <cassert>

#include "LinAlg.h"
#include "Point3.h"
#include "Vector3.h"

namespace nanoRVE {

/**
 * \brief A 3x3 matrix.
 *
 * The matrix is stored in column-major order. Matrix_3 is derived from std::array< Vector_3<T>, 3 >. Thus, it is an array of three column vectors.
  */
template<typename T>
class Matrix_3 : public std::array<Vector_3<T>,3>
{
public:

	/// An empty type that denotes a 3x3 matrix with all elements equal to zero.
	struct Zero {};

	/// An empty type that denotes the 3x3 identity matrix.
	struct Identity {};

	/// The type of a single element of the matrix.
	typedef T element_type;

	/// The type of a single column of the matrix.
	typedef Vector_3<T> column_type;

	using typename std::array<Vector_3<T>, 3>::size_type;
	using typename std::array<Vector_3<T>, 3>::difference_type;
	using typename std::array<Vector_3<T>, 3>::value_type;
	using typename std::array<Vector_3<T>, 3>::iterator;
	using typename std::array<Vector_3<T>, 3>::const_iterator;

public:

	/// \brief Empty default constructor that does not initialize the matrix elements (for performance reasons).
	///        The matrix elements will have an undefined value and need to be initialized later.
	Matrix_3() {}

	/// \brief Constructor that initializes all 9 elements of the matrix to the given values.
	/// \note Values are given in row-major order, i.e. row by row.
	constexpr Matrix_3(T m11, T m12, T m13,
					   T m21, T m22, T m23,
					   T m31, T m32, T m33)
		: std::array<Vector_3<T>,3>{{Vector_3<T>(m11,m21,m31),
									 Vector_3<T>(m12,m22,m32),
									 Vector_3<T>(m13,m23,m33)}} {}

	/// \brief Constructor that initializes the matrix from three column vectors.
	constexpr Matrix_3(const column_type& c1, const column_type& c2, const column_type& c3)
		: std::array<Vector_3<T>,3>{{c1, c2, c3}} {}

	/// \brief Initializes the matrix to the null matrix.
	/// All matrix elements are set to zero by this constructor.
	constexpr Matrix_3(Zero)
		: std::array<Vector_3<T>,3>{{typename Vector_3<T>::Zero(), typename Vector_3<T>::Zero(), typename Vector_3<T>::Zero()}} {}

	/// \brief Initializes the matrix to the identity matrix.
	/// All diagonal elements are set to one, and all off-diagonal elements are set to zero.
	constexpr Matrix_3(Identity)
		: std::array<Vector_3<T>,3>{{Vector_3<T>(T(1),T(0),T(0)),
									 Vector_3<T>(T(0),T(1),T(0)),
									 Vector_3<T>(T(0),T(0),T(1))}} {}

	/// \brief Casts the matrix to a matrix with another data type.
	template<typename U>
	constexpr explicit operator Matrix_3<U>() const {
		return Matrix_3<U>(
				static_cast<U>((*this)(0,0)), static_cast<U>((*this)(0,1)), static_cast<U>((*this)(0,2)),
				static_cast<U>((*this)(1,0)), static_cast<U>((*this)(1,1)), static_cast<U>((*this)(1,2)),
				static_cast<U>((*this)(2,0)), static_cast<U>((*this)(2,1)), static_cast<U>((*this)(2,2)));
	}

	/// \brief Returns the number of rows of this matrix.
	static constexpr size_type row_count() { return 3; }

	/// \brief Returns the number of columns of this matrix.
	static constexpr size_type col_count() { return 3; }

	/// \brief Returns the value of a matrix element.
	/// \param row The matrix row of the element.
	/// \param col The matrix column of the element.
	/// \return The value of the matrix element.
	constexpr inline T operator()(size_type row, size_type col) const {
		return (*this)[col][row];
	}

	/// \brief Returns a reference to a matrix element.
	/// \param row The row of the element.
	/// \param col The column of the element.
	/// \return A non-const reference to the matrix element, which may be written to.
	inline T& operator()(size_type row, size_type col) {
		return (*this)[col][row];
	}

	/// \brief Returns a column vector of the matrix.
	/// \param col The index of the column to return.
	/// \return A vector containing the matrix elements of the column \a col.
	constexpr const column_type& column(size_type col) const {
		return (*this)[col];
	}

	/// \brief Returns a reference to a column vector of the matrix.
	/// \param col The column to return.
	/// \return A reference to the vector containing the matrix elements of the column \a col.
	/// \note Modifying the elements of the returned vector will modify the matrix elements.
	/// \sa row()
	column_type& column(size_type col) {
		return (*this)[col];
	}

	/// \brief Returns a row from the matrix.
	/// \param row The row to return.
	/// \return The requested row of the matrix as a vector.
	/// \note The returned vector is a copy of the matrix row elements.
	/// \sa column()
	constexpr Vector_3<T> row(size_type row) const {
		return { (*this)[0][row], (*this)[1][row], (*this)[2][row] };
	}

	/// Returns a pointer to the 9 elements of the matrix (stored in column-major order).
	const element_type* elements() const {
		return column(0).data();
	}

	/// Returns a pointer to the 9 elements of the matrix (stored in column-major order).
	element_type* elements() {
		return column(0).data();
	}

	/// Sets all elements of the matrix to zero.
	void setZero() {
		(*this)[0].setZero();
		(*this)[1].setZero();
		(*this)[2].setZero();
	}

	/// Sets all elements of the matrix to zero.
	Matrix_3& operator=(Zero) {
		setZero();
		return *this;
	}

	/// Sets the matrix to the identity matrix.
	void setIdentity() {
		(*this)[0] = Vector_3<T>(1,0,0);
		(*this)[1] = Vector_3<T>(0,1,0);
		(*this)[2] = Vector_3<T>(0,0,1);
	}

	/// Sets the matrix to the identity matrix.
	Matrix_3& operator=(Identity) {
		setIdentity();
		return *this;
	}

	////////////////////////////////// Comparison ///////////////////////////////////

	/// \brief Compares two matrices for exact equality.
	/// \return \c true if all elements are equal; \c false otherwise.
	constexpr bool operator==(const Matrix_3& b) const {
		return (b[0] == (*this)[0]) && (b[1] == (*this)[1]) && (b[2] == (*this)[2]);
	}

	/// \brief Compares two matrices for inequality.
	/// \return \c true if not all elements are equal; \c false if all are equal.
	constexpr bool operator!=(const Matrix_3& b) const {
		return !(*this == b);
	}

	/// \brief Tests if two matrices are equal within a given tolerance.
	/// \param m The matrix to compare to.
	/// \param tolerance A non-negative threshold for the equality test. The two matrices are considered equal if
	///        the element-wise differences are all less than this tolerance value.
	/// \return \c true if this matrix is equal to \a m within the given tolerance; \c false otherwise.
	inline bool equals(const Matrix_3& m, T tolerance = T(FLOATTYPE_EPSILON)) const {
		for(size_type i = 0; i < col_count(); i++)
			if(!column(i).equals(m.column(i), tolerance)) return false;
		return true;
	}

	/// \brief Test if the matrix is zero within a given tolerance.
	/// \param tolerance A non-negative threshold.
	/// \return \c true if the absolute value of each matrix element is all smaller than \a tolerance.
	inline bool isZero(T tolerance = T(FLOATTYPE_EPSILON)) const {
		for(size_type i = 0; i < col_count(); i++)
			if(!column(i).isZero(tolerance)) return false;
		return true;
	}

	////////////////////////////////// Computations ///////////////////////////////////

	/// Computes the inverse of the matrix.
	/// \throw Exception if matrix is not invertible because it is singular.
	/// \sa determinant()
	Matrix_3 inverse() const {
		T det = determinant();
		assert(det != T(0));
		return Matrix_3(((*this)[1][1]*(*this)[2][2] - (*this)[1][2]*(*this)[2][1])/det,
						((*this)[2][0]*(*this)[1][2] - (*this)[1][0]*(*this)[2][2])/det,
						((*this)[1][0]*(*this)[2][1] - (*this)[1][1]*(*this)[2][0])/det,
						((*this)[2][1]*(*this)[0][2] - (*this)[0][1]*(*this)[2][2])/det,
						((*this)[0][0]*(*this)[2][2] - (*this)[2][0]*(*this)[0][2])/det,
						((*this)[0][1]*(*this)[2][0] - (*this)[0][0]*(*this)[2][1])/det,
						((*this)[0][1]*(*this)[1][2] - (*this)[1][1]*(*this)[0][2])/det,
						((*this)[0][2]*(*this)[1][0] - (*this)[0][0]*(*this)[1][2])/det,
						((*this)[0][0]*(*this)[1][1] - (*this)[1][0]*(*this)[0][1])/det);
	}

	/// \brief Computes the inverse of the matrix.
	/// \param result A reference to an output matrix that will receive the computed inverse.
	/// \param epsilon A threshold that is used to determine if the matrix is invertible. The matrix is considered singular if |det|<=epsilon.
	/// \return \c false if the matrix is not invertible because it is singular; \c true if the inverse has been calculated
	///         and was stored in \a result.
	/// \sa determinant()
	bool inverse(Matrix_3& result, T epsilon = T(FLOATTYPE_EPSILON)) const {
		T det = determinant();
		if(std::abs(det) <= epsilon) return false;
		result = Matrix_3(((*this)[1][1]*(*this)[2][2] - (*this)[1][2]*(*this)[2][1])/det,
						((*this)[2][0]*(*this)[1][2] - (*this)[1][0]*(*this)[2][2])/det,
						((*this)[1][0]*(*this)[2][1] - (*this)[1][1]*(*this)[2][0])/det,
						((*this)[2][1]*(*this)[0][2] - (*this)[0][1]*(*this)[2][2])/det,
						((*this)[0][0]*(*this)[2][2] - (*this)[2][0]*(*this)[0][2])/det,
						((*this)[0][1]*(*this)[2][0] - (*this)[0][0]*(*this)[2][1])/det,
						((*this)[0][1]*(*this)[1][2] - (*this)[1][1]*(*this)[0][2])/det,
						((*this)[0][2]*(*this)[1][0] - (*this)[0][0]*(*this)[1][2])/det,
						((*this)[0][0]*(*this)[1][1] - (*this)[1][0]*(*this)[0][1])/det);
		return true;
	}

	/// Calculates the determinant of the matrix.
	constexpr inline T determinant() const {
		return(((*this)[0][0]*(*this)[1][1] - (*this)[0][1]*(*this)[1][0])*((*this)[2][2])
			  -((*this)[0][0]*(*this)[1][2] - (*this)[0][2]*(*this)[1][0])*((*this)[2][1])
			  +((*this)[0][1]*(*this)[1][2] - (*this)[0][2]*(*this)[1][1])*((*this)[2][0]));
	}

	/// Returns the transpose of the matrix.
	constexpr Matrix_3 transposed() const {
		return Matrix_3((*this)[0][0], (*this)[0][1], (*this)[0][2],
						(*this)[1][0], (*this)[1][1], (*this)[1][2],
						(*this)[2][0], (*this)[2][1], (*this)[2][2]);
	}

	/// Computes the product of the matrix and a point and returns one coordinate of the resulting point.
	/// \param p The point to transform with the matrix.
	/// \param index The component (0-2) of the transformed point to return.
	/// \return ((*this)*p)[index]
	inline constexpr T prodrow(const Point_3<T>& p, typename Point_3<T>::size_type index) const {
		return (*this)[0][index] * p[0] + (*this)[1][index] * p[1] + (*this)[2][index] * p[2];
	}

	/// Computes the product of the matrix and a vector and returns one component of the resulting vector.
	/// \param v The vector to transform with the matrix.
	/// \param index The component (0-2) of the transformed vector to return.
	/// \return ((*this)*v)[index]
	inline constexpr T prodrow(const Vector_3<T>& v, typename Vector_3<T>::size_type index) const {
		return (*this)[0][index] * v[0] + (*this)[1][index] * v[1] + (*this)[2][index] * v[2];
	}

	/// \brief Tests whether the matrix is a pure rotation-reflection matrix (i.e. orthogonal matrix).
	/// \return \c true if the matrix is orthogonal; \c false otherwise.
	///
	/// The matrix A is orthogonal matrix if A * A^T = I.
	constexpr bool isOrthogonalMatrix(T epsilon = T(FLOATTYPE_EPSILON)) const {
		return
			(std::abs((*this)[0][0]*(*this)[1][0] + (*this)[0][1]*(*this)[1][1] + (*this)[0][2]*(*this)[1][2]) <= epsilon) &&
			(std::abs((*this)[0][0]*(*this)[2][0] + (*this)[0][1]*(*this)[2][1] + (*this)[0][2]*(*this)[2][2]) <= epsilon) &&
			(std::abs((*this)[1][0]*(*this)[2][0] + (*this)[1][1]*(*this)[2][1] + (*this)[1][2]*(*this)[2][2]) <= epsilon) &&
			(std::abs((*this)[0][0]*(*this)[0][0] + (*this)[0][1]*(*this)[0][1] + (*this)[0][2]*(*this)[0][2] - T(1)) <= epsilon) &&
			(std::abs((*this)[1][0]*(*this)[1][0] + (*this)[1][1]*(*this)[1][1] + (*this)[1][2]*(*this)[1][2] - T(1)) <= epsilon) &&
			(std::abs((*this)[2][0]*(*this)[2][0] + (*this)[2][1]*(*this)[2][1] + (*this)[2][2]*(*this)[2][2] - T(1)) <= epsilon);
	}

	/// \brief Tests whether the matrix is a pure rotation matrix.
	/// \return \c true if the matrix is a pure rotation matrix; \c false otherwise.
	///
	/// The matrix A is a pure rotation matrix if:
	///   1. det(A) = 1  and
	///   2. A * A^T = I
	constexpr bool isRotationMatrix(T epsilon = T(FLOATTYPE_EPSILON)) const {
		return isOrthogonalMatrix(epsilon) && (std::abs(determinant() - T(1)) <= epsilon);
	}

	/// Orthonormalizes the matrix.
	///
    /// Algorithm uses Gram-Schmidt orthogonalization.  If this matrix is
    /// M = [m0|m1|m2], then the orthonormal output matrix is Q = [q0|q1|q2], with
    ///
    ///     q0 = m0/|m0|
    ///     q1 = (m1-(q0*m1)q0)/|m1-(q0*m1)q0|
    ///     q2 = (m2-(q0*m2)q0-(q1*m2)q1)/|m2-(q0*m2)q0-(q1*m2)q1|
    ///
    /// where |V| denotes length of vector V and A*B denotes dot
    /// product of vectors A and B.
	void orthonormalize() {

		// Compute q0.
		(*this)[0].normalize();

	    // Compute q1.
		T dot0 = (*this)[0].dot((*this)[1]);
		(*this)[1][0] -= dot0 * (*this)[0][0];
		(*this)[1][1] -= dot0 * (*this)[0][1];
		(*this)[1][2] -= dot0 * (*this)[0][2];
		(*this)[1].normalize();

	    // compute q2
	    dot0 = (*this)[0].dot((*this)[2]);
	    T dot1 = (*this)[1].dot((*this)[2]);
	    (*this)[2][0] -= dot0*(*this)[0][0] + dot1*(*this)[1][0];
	    (*this)[2][1] -= dot0*(*this)[0][1] + dot1*(*this)[1][1];
	    (*this)[2][2] -= dot0*(*this)[0][2] + dot1*(*this)[1][2];
	    (*this)[2].normalize();
	}

};

/// \brief Computes the product of a matrix and a vector.
/// \relates Matrix_3
template<typename T>
constexpr inline Vector_3<T> operator*(const Matrix_3<T>& m, const Vector_3<T>& v)
{
	return { m(0,0)*v[0] + m(0,1)*v[1] + m(0,2)*v[2],
			 m(1,0)*v[0] + m(1,1)*v[1] + m(1,2)*v[2],
			 m(2,0)*v[0] + m(2,1)*v[1] + m(2,2)*v[2] };
}

/// \brief Computes the product of a matrix and a point. This is the same as a matrix-vector product.
/// \relates Matrix_3
template<typename T>
constexpr inline Point_3<T> operator*(const Matrix_3<T>& m, const Point_3<T>& p)
{
	return { m(0,0)*p[0] + m(0,1)*p[1] + m(0,2)*p[2],
			 m(1,0)*p[0] + m(1,1)*p[1] + m(1,2)*p[2],
			 m(2,0)*p[0] + m(2,1)*p[1] + m(2,2)*p[2] };
}

/// \brief Computes the product of two matrices.
/// \relates Matrix_3
template<typename T>
constexpr inline Matrix_3<T> operator*(const Matrix_3<T>& a, const Matrix_3<T>& b)
{
#if 1
	return Matrix_3<T>(
			a(0,0)*b(0,0) + a(0,1)*b(1,0) + a(0,2)*b(2,0),
			a(0,0)*b(0,1) + a(0,1)*b(1,1) + a(0,2)*b(2,1),
			a(0,0)*b(0,2) + a(0,1)*b(1,2) + a(0,2)*b(2,2),

			a(1,0)*b(0,0) + a(1,1)*b(1,0) + a(1,2)*b(2,0),
			a(1,0)*b(0,1) + a(1,1)*b(1,1) + a(1,2)*b(2,1),
			a(1,0)*b(0,2) + a(1,1)*b(1,2) + a(1,2)*b(2,2),

			a(2,0)*b(0,0) + a(2,1)*b(1,0) + a(2,2)*b(2,0),
			a(2,0)*b(0,1) + a(2,1)*b(1,1) + a(2,2)*b(2,1),
			a(2,0)*b(0,2) + a(2,1)*b(1,2) + a(2,2)*b(2,2)
	);
#else
	Matrix_3<T> m;
	for(typename Matrix_3<T>::size_type col = 0; col < 3; col++) {
		for(typename Matrix_3<T>::size_type row = 0; row < 3; row++) {
			T v{0};
			for(typename Matrix_3<T>::size_type k = 0; k < 3; k++)
				v += a(row, k) * b(k, col);
			m(row, col) = v;
		}
	}
	return m;
#endif
}


/// \brief Subtracts a 3x3 matrix from a 3x3 Matrix.
template<typename T>
inline Matrix_3<T> operator-(const Matrix_3<T>& a, const Matrix_3<T>& b)
{
	Matrix_3<T> m;
	for(typename Matrix_3<T>::size_type col = 0; col < 3; col++) {
		for(typename Matrix_3<T>::size_type row = 0; row < 3; row++) {
			m(row, col) = a(row, col) - b(row, col);
		}
	}
	return m;
}



/// \brief Multiplies a matrix with a scalar value.
/// \relates Matrix_3
template<typename T>
constexpr inline Matrix_3<T> operator*(const Matrix_3<T>& a, T s)
{
#if 1
	return Matrix_3<T>(
			a(0,0)*s, a(0,1)*s, a(0,2)*s,
			a(1,0)*s, a(1,1)*s, a(1,2)*s,
			a(2,0)*s, a(2,1)*s, a(2,2)*s
	);
#else
	Matrix_3<T> m;
	for(typename Matrix_3<T>::size_type i = 0; i < 3; i++) {
		for(typename Matrix_3<T>::size_type j = 0; j < 3; j++) {
			m(i, j) = a(i, j) * s;
		}
	}
	return m;
#endif
}

/// \brief Multiplies a matrix with a scalar value.
/// \relates Matrix_3
template<typename T>
constexpr inline Matrix_3<T> operator*(T s, const Matrix_3<T>& a) {
	return a * s;
}

/// \brief Prints a matrix to a text output stream.
/// \relates Matrix_3
template<typename T>
inline std::ostream& operator<<(std::ostream &os, const Matrix_3<T>& m) {
	for(typename Matrix_3<T>::size_type row = 0; row < m.row_count(); row++)
		os << m.row(row) << std::endl;
	return os;
}

/**
 * \brief Instantiation of the Matrix_3 class template with the default floating-point type.
 * \relates Matrix_3
 */
using Matrix3 = Matrix_3<FloatType>;

} // End of namespace
