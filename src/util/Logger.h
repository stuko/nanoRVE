///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <sstream>

namespace nanoRVE {

/// Levels of program output verbosity.
enum LoggerVerbosity {
  none,
  minimum,
  medium,
  maximum,
  debug
};

class MsgLogger
{
public:

	/// Constructor.
	MsgLogger(LoggerVerbosity verb) : _enabled(verb <= _globalLoggerVerbosity) {}

	/// Destructor.
    ~MsgLogger() { std::cout << _buffer.str(); }

	template<typename T>
	MsgLogger& operator<<(T const& t) {
		if(_enabled)
			_buffer << t;
		return *this;
	}

	MsgLogger& operator<<( std::ostream&(*f)(std::ostream&) ) {
		if(_enabled)
			_buffer << f;
		return *this;
	}

	bool isEnabled() const { return _enabled; }

	/// Changes the verbosity threshold level of the logger stream.
	static void setVerbosity(LoggerVerbosity verb) {
		_globalLoggerVerbosity = verb;
	}

private:

	bool _enabled;
	std::ostringstream _buffer;

	// The current verbosity level.
	static LoggerVerbosity _globalLoggerVerbosity;
};


/// This stream manipulator function outputs a separator line to the output stream.
inline std::ostream& separatorLine(std::ostream& stream) {
	return stream << "-------------------------------------------------------" << std::endl;
}

} // End of namespace
