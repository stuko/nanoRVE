///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace nanoRVE {

// Use 64-bit double as default floating point type.
typedef double FloatType;

// This tells the program that we're using 64-bit floating point.
#define FLOATTYPE_DOUBLE

/// A small epsilon value for the FloatType.
#define FLOATTYPE_EPSILON	1e-9

// These are the format strings used with the sscanf() parsing function.
// We have to obey the floating point precision used to compile the program,
// because the destination variables are either single or double precision variables.
#define FLOAT_SCANF_STRING_1   "%lg"
#define FLOAT_SCANF_STRING_2   "%lg %lg"
#define FLOAT_SCANF_STRING_3   "%lg %lg %lg"

/// Computes the square of a number.
template<typename T> inline T square(const T& f) { return f*f; }

/// The maximum value for floating point variables.
#define FLOATTYPE_MAX	(std::numeric_limits<FloatType>::max())

// The Pi constant
#define FLOATTYPE_PI	((FloatType)3.14159265358979323846)

// The square root of 2*pi
#define FLOATTYPE_SQRTTWOPI    ((FloatType)2.50662827463100050241)

} // End of namespace
