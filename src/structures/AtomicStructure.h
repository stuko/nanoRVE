///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../nanoRVE.h"
#include "NeighborList.h"

namespace nanoRVE {

/**
* @brief      Base class for maintaining structures.
*
* @details    This class is used to store structure specific information
*             including atomic coordinates, neighbor lists, total energy,
*             volume, pressure etc.
*/
class AtomicStructure
{
public:

	/**
	* @brief Flags that indicate which parts of the structure need to be updated prior to the next energy calculation.
	*/
	enum DirtyFlags {
		/// neighbor lists require updating
		NEIGHBOR_LISTS = (1<<0),
		/// ghost atom positions require updating
		ATOM_POSITIONS = (1<<1),
		/// degree(s) of freedom requires updating
		STRUCTURE_DOF = (1<<4),
	};

public:

	/*************************************** Constructor / destructor ***********************************/

	AtomicStructure() = default;

	/********************************************* Atom types *******************************************/

	/// Returns the number of atom types defined.
	int numAtomTypes() const { return _natomtypes; }

	/// Adds a new atom type definition.
	bool addAtomType(const std::string& name, double mass = 0.0, int atomicNumber = 0) {
		if(name.empty() || std::find(_atomTypeNames.begin(), _atomTypeNames.end(), name) != _atomTypeNames.end())
			return false;
		_atomTypeNames.push_back(name);
		_atomTypeMasses.push_back(mass);
		_atomTypeAtomicNumbers.push_back(atomicNumber);
		_natomtypes = (int)_atomTypeNames.size();
		return true;
	}

	/// Returns the name of the i-th atom type.
	const std::string& atomTypeName(int i) const {
		assert(_natomtypes == _atomTypeNames.size());
		assert(i >= 0 && i < _atomTypeNames.size());
		return _atomTypeNames[i];
	}

	/// Returns the mass of the i-th atom type.
	/// May be zero if not set.
	double atomTypeMass(int i) const {
		assert(_natomtypes == _atomTypeMasses.size());
		assert(i >= 0 && i < _atomTypeMasses.size());
		return _atomTypeMasses[i];
	}

	/// Returns the atomic of the i-th atom type.
	/// May be zero if not set.
	int atomTypeAtomicNumber(int i) const {
		assert(_natomtypes == _atomTypeAtomicNumbers.size());
		assert(i >= 0 && i < _atomTypeAtomicNumbers.size());
		return _atomTypeAtomicNumbers[i];
	}

	/// Returns the index of the atom type.
	int atomTypeIndex(const std::string& s) const {
		auto iter = std::find(_atomTypeNames.begin(), _atomTypeNames.end(), s);
        assert(iter != _atomTypeNames.end());
		return iter - _atomTypeNames.begin();
	}

	/// Returns the maximum cutoff radius of all potentials used by this structure.
	double maximumCutoff() const { return _maximumCutoff; }

	/********************************************** Potentials *******************************************/

	/// Returns the list of all potentials used in this structure.
	const std::vector<std::unique_ptr<Potential>>& potentials() const { return _potentials; }
	
	/*************************************** Simulation cell geometry ************************************/

	/**
	 @brief Set up the simulation cell.
	 @param cellVectors cell metric (3x3 matrix)
	 @param cellOrigin origin of cell (3-dim vector)
	 @param pbc periodic boundary conditions in each direction
	*/
	void setupSimulationCell(const Matrix3& cellVectors, const Point3& cellOrigin = Point3::Origin(), const std::array<bool,3>& pbc = std::array<bool,3>{{true,true,true}});

	/**
	   @brief Applies an affine transformation to the cell and the atoms.
	   @param deformation defined 3x3 deformation matrix that is applied to the simulation cell
	**/
	void deformSimulationCell(const Matrix3& deformation);

	/**
	   @brief Returns global simulation cell matrix.
	   @return 3x3 matrix representing the simulation cell metric
	 **/
	const Matrix3& simulationCell() const { return _simulationCell; }

	/**
	   @brief The origin point of the global simulation cell in world coordinates.
	   @return 3-dimensional vector representing the origin of the simulation cell.
	**/
	const Point3& simulationCellOrigin() const { return _simulationCellOrigin; }

	/**
	   @brief The inverse of the simulation cell matrix used to transform absolute coordinates to reduced coordinates.
	   @return 3x3 matrix representing the reciprocal cell metric
	**/
	const Matrix3& reciprocalSimulationCell() const { return _reciprocalSimulationCell; }

	/**
	  	@brief Returns whether periodic boundary conditions are enabled in the given spatial dimension.
	  	@param dimension index to spatial dimension (0=x, 1=y, z=3)
	  	@return True/False if boundary condition is enabled/disabled.
	 **/
	bool hasPBC(size_t dimension) const { return _pbc[dimension]; }

	/**
	  	@brief Returns the periodic boundary condition flags.
	 	@return 3-dimensional vector of True/False values.
	 */
	const std::array<bool,3>& pbc() const { return _pbc; }

	/*************************************** Coordinate utility functions ************************************/

	/// Determines the periodic image of the cell the given point is located in.
	Vector3I periodicImage(const Point3& p) const;

	/// Wraps a point to be inside the simulation cell if periodic boundary conditions are enabled.
	Point3 wrapPoint(Point3 p) const;

	/// Wraps a point given in reduced coordinated to be inside the simulation cell.
	Point3 wrapReducedPoint(Point3 p) const;

	/// Converts a point given in reduced cell coordinates to a point in absolute coordinates.
	Point3 reducedToAbsolute(const Point3& reducedPoint) const { return simulationCellOrigin() + (simulationCell() * (reducedPoint - Point3::Origin())); }

	/// Converts a point given in absolute coordinates to a point in reduced cell coordinates.
	Point3 absoluteToReduced(const Point3& worldPoint) const { return Point3::Origin() + (reciprocalSimulationCell() * (worldPoint - simulationCellOrigin())); }

	/// Converts a vector given in reduced cell coordinates to a vector in absolute coordinates.
	Vector3 reducedToAbsolute(const Vector3& reducedVec) const { return simulationCell() * reducedVec; }

	/// Converts a vector given in absolute coordinates to a point in vector cell coordinates.
	Vector3 absoluteToReduced(const Vector3& worldVec) const { return reciprocalSimulationCell() * worldVec; }

	/************************************************ Atoms ***********************************************/

	/**
	   @brief Resizes the atoms array.
	   @param numLocalAtoms new number of (local) atoms.
	 */
	void setAtomCount(int numLocalAtoms);

	/// Returns the number of (real) atoms in the structure cell.
	int numLocalAtoms() const { return _numLocalAtoms; }

	/// Returns the number of ghost atoms in the structure cell.
	int numGhostAtoms() const { return _numGhostAtoms; }

	/// Returns the total number of atoms in the structure cell including real and ghost atoms.
	int numAtoms() const { return _numAtoms; }

	/// Returns the array of positions of all atoms (including ghosts) in the structure (const version).
	const std::vector<Point3>& atomPositions() const { return _atomPositions; }

	/// Returns the array of positions of all atoms (including ghosts) in the structure cell (non-const version).
	std::vector<Point3>& atomPositions() { return _atomPositions; }

	/**
	   @brief Sets the array of positions of all atoms in the structure cell.
	   @param newPositions vector of 3-dimensional vectors ("points") representing the atomic coordinates. Size must match the number of local atoms.
	 */
	void setAtomPositions(const std::vector<Point3>& newPositions) {
		assert(newPositions.size() == numLocalAtoms());
		std::copy(newPositions.begin(), newPositions.end(), _atomPositions.begin());
		setDirty(ATOM_POSITIONS);
	}

	/// Returns the array of displacements of all atoms in the structure cell wrt a reference structure (const version).
	const std::vector<Point3>& atomDisplacements() const { return _atomDisplacements; }

	/// Returns the array of displacements of all atoms in the structure cell wrt a reference structure (non-const version).
	std::vector<Point3>& atomDisplacements() { return _atomDisplacements; }

	/// Returns the initial positions of the all real atoms they had at the beginning of the job.
	const std::vector<Point3>& atomInitialPositions() const { return _atomInitialPositions; }

	/// Returns the type of a i-th atom.
	int atomType(int i) const { return _atomTypes[i]; }

	/// Returns the array of types of all atoms in the cell (const version).
	const std::vector<int>& atomTypes() const { return _atomTypes; }

	/// Returns the array of types of all atoms in the cell (non-const version).
	std::vector<int>& atomTypes() { return _atomTypes; }

	/// Returns the array of IDs of the atoms in the cell (const version).
	const std::vector<int>& atomTags() const { return _atomTags; }

	/// Returns the array of IDs of the atoms in the cell (non-const version).
	std::vector<int>& atomTags() { return _atomTags; }

	/// Returns the array of force vectors (const version).
	const std::vector<Vector3>& atomForces() const { return _atomForces; }

	/// Returns the array of force vectors (non-const version).
	std::vector<Vector3>& atomForces() { return _atomForces; }

	/// Returns an array of integer pairs that map the real atoms to the ghost atoms.
	const std::vector< std::pair<int,int> >& forwardMapping() const { return _forwardMapping; }

	/// Returns an array of integers that maps the ghost atoms to the real atoms.
	const std::vector<int>& reverseMapping() const { return _reverseMapping; }

	/// Returns a pointer to the memory buffer that stores temporary per-atom data used by the potential routines.
	template<typename T>
	T* perAtomData() { return _perAtomPotentialData.empty() ? nullptr : reinterpret_cast<T*>(&_perAtomPotentialData.front()); }

	// Returns the neighbor list built for this structure and the given potential.
	NeighborList& neighborList(const Potential* potential) {
		auto nl = _neighborLists.find(potential);
		assert(nl != _neighborLists.end());
		return nl->second;
	}

	/************************************ Updating and modifying the structure ***************************************/

	/// Marks parts of the structure that must be updated before the next energy calculation.
	void setDirty(DirtyFlags flags) { _dirtyFlags |= flags; }

	/// Tests whether certain parts of the structure must be updated before the next energy calculation.
	bool isDirty(DirtyFlags parts) const { return (_dirtyFlags & parts) != 0; }

	/// Resets the dirty flags.
	void clearDirty(DirtyFlags parts) { _dirtyFlags &= ~parts; }

	/**
	   @brief Updates the structure, creates ghost atoms and builds neighbor lists.
	*/
	virtual void updateStructure();

	/******************************************** Property calculation ***********************************************/

	/**
	   @brief Computes the total energy and optionally the forces for this structure.
	   @param computeForces Turns on the computation of forces.
	   @param isFitting Indicates that the structure is included in the fitting data set.
	   @param suppressRelaxation If True structure relaxation will be suppressed.
	   @return A scalar representing the energy of the structure. If the forces are computed they are stored in the structure object.
	   @see atomForcesProperty() for retrieving the atomic forces
	*/
	double computeEnergy(bool computeForces, bool isFitting, bool suppressRelaxation = false);

	/******************************************** Input/output ***********************************************/

	/**
	 @brief Exports the structure to a LAMMPS dump file.
	 @param filename Name of the output file
	 @param includeGhostAtoms If True ghost atoms used during computation of the energy will be included in the output
	*/
	void writeToDumpFile(const std::string& filename, bool includeGhostAtoms = false) const;

	/**
	 @brief Exports the structure to a POSCAR file.
	 @param filename Name of the output file
	 @param includeGhostAtoms If True ghost atoms used during computation of the energy will be included in the output
	*/
	void writeToPoscarFile(const std::string& filename, bool includeGhostAtoms = false) const;

	/// Loads a LAMMPS dump file into the user structure.
	void loadLAMMPSDumpFile(const std::string& filepath);
	
	/// Loads a POSCAR file into the user structure.
	void loadPOSCARFile(const std::string& filepath);

	/************************************* Access to calculated quantities *****************************************/

	/// Returns the total potential energy of this structure after computeEnergy() has been called.
	double totalEnergy() const { return _totalEnergy; }

	/// Returns the virial tensor computed by the force routine.
	const std::array<double,6>& virial() const { return _virial; }

	/**
	   @brief Returns a reference to the virial tensor.
	   @details The virial tensor is returned as a six-dimensional vector. The indices of the vector follow the <a href="http://en.wikipedia.org/wiki/Voigt_notation">Voigt notation</a>.
	*/
	std::array<double,6>& virial() { return _virial; }

private:

	/**
	   @brief Compute the elastic constants of the structure.
	   @details This function triggers the computation of the elastic constants if the corresponding properties are enabled.
	**/
	void computeElasticConstants(bool isFitting);

	/// Indicate which parts of the structure must be updated before the next energy calculation.
	int _dirtyFlags = NEIGHBOR_LISTS | ATOM_POSITIONS | STRUCTURE_DOF;

	/// The simulation cell matrix.
	Matrix3 _simulationCell;
	/// The origin point of the simulation cell in world coordinates.
	Point3 _simulationCellOrigin;
	/// The inverse of the simulation cell matrix used to transform absolute coordinates to reduced coordinates.
	Matrix3 _reciprocalSimulationCell;
	/// Indicates whether periodic boundary conditions are enabled for each of the three spatial directions.
	std::array<bool,3> _pbc;

	/// The number of (real) atoms in the structure cell.
	int _numLocalAtoms = 0;
	/// The number of ghost atoms.
	int _numGhostAtoms = 0;
	/// The total number of atoms including real and ghost atoms.
	int _numAtoms = 0;
	/// The neighbor list skin thickness.
	double _skinThickness = 0;
	/// The positions of all atoms in the structure (including ghost atoms).
	std::vector<Point3> _atomPositions;
	/// The positions of the real atoms in the structure the last time the neighbor lists and ghost atoms have been generated.
	std::vector<Point3> _lastNeighborUpdatePositions;
	/// The simulation cell matrix last time the neighbor lists have been built.
	Matrix3 _lastSimulationCell;
	/// Stores the number of PBC ghost atom images in each spatial direction that were generated.
	Vector3I _ghostAtomImages;
	/// The displacements of the all atoms in the structure cell with respect to a reference structure.
	std::vector<Point3> _atomDisplacements;
	/// The initial positions of the all real atoms they had at the beginning of the job.
	std::vector<Point3> _atomInitialPositions;
	/// The types of all atoms in the cell (including ghost atoms).
	std::vector<int> _atomTypes;
	/// The IDs of the atoms in the cell (including ghost atoms).
	std::vector<int> _atomTags;
	/// Maps real atom indices to ghost atom indices.
	std::vector<std::pair<int,int>> _forwardMapping;
	/// Maps the ghost atoms to the real atoms.
	std::vector<int> _reverseMapping;
	/// Working memory buffer for temporary per-atom data used by the potential routines.
	std::vector<unsigned char> _perAtomPotentialData;
	/// Stores the PBC image for every ghost atom.
	std::vector<Vector3> _ghostAtomPBCImages;

	/// The number of atom types used in this job.
	int _natomtypes = 0;
	
	/// The identifiers/names assigned to the atom types.
	/// This is a 0-based array.
	std::vector<std::string> _atomTypeNames;

	/// The masses assigned to the atom types (may be zero if not defined).
	/// This is a 0-based array.
	std::vector<double> _atomTypeMasses;

	/// The atomic numbers assigned to the atom types.
	/// This is a 0-based array.
	std::vector<int> _atomTypeAtomicNumbers;
		
	/// The maximum cutoff radius of all potentials used in this structure.
	double _maximumCutoff = 0.0;
	
	/// Stores the neighbor list for each potential.
	std::map<const Potential*, NeighborList> _neighborLists;

	/// The total potential energy of this structure.
	double _totalEnergy;

    /// Itercount of last atom position update
    int _lastUpdate = 0;

	/// The accumulated virial computed by the force routine.
	std::array<double,6> _virial;

	/// The force vector.
	std::vector<Vector3> _atomForces;

	/// List of all potentials used in this structure.
	std::vector<std::unique_ptr<Potential>> _potentials;
};

} // End of namespace
