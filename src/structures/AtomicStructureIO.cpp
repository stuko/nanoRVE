///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructure.h"
#include "../potentials/Potential.h"
#include "../minimizers/LBFGSMinimizer.h"

namespace nanoRVE {

using namespace std;

/******************************************************************************
 * Helper function that checks whether a string starts with a certain sequence
 * of characters.
 ******************************************************************************/
static bool starts_with(const string& str, const char* text)
{
	auto iter = str.begin();
	while(iter != str.end() && *text != '\0') {
		if(*iter != *text) return false;
		++iter;
		++text;
	}
	return *text != '\0' || iter == str.end();
}

/******************************************************************************
 * Exports the structure to a POSCAR file.
 ******************************************************************************/
void AtomicStructure::writeToPoscarFile(const std::string& filename, bool includeGhostAtoms) const
{
	ofstream stream(filename.c_str());
	if(!stream.is_open())
		throw runtime_error("Could not open POSCAR file for writing.");

	stream << "POSCAR file written by nanoRVE" << endl;
    stream << "   1.0" << endl;
	stream << fixed << setprecision(16);

    // Write simulation cell.
    stream << " " << setw(22) << simulationCell()(0,0) << setw(22) << simulationCell()(1,0) << setw(22) << simulationCell()(2,0) << endl;
    stream << " " << setw(22) << simulationCell()(0,1) << setw(22) << simulationCell()(1,1) << setw(22) << simulationCell()(2,1) << endl;
    stream << " " << setw(22) << simulationCell()(0,2) << setw(22) << simulationCell()(1,2) << setw(22) << simulationCell()(2,2) << endl;

    // Count atoms of each type.
	vector<int> typeCount(numAtomTypes(), 0);
    int natoms = includeGhostAtoms ? numAtoms() : numLocalAtoms();
	for(int i = 0; i < natoms; i++) {
		typeCount[atomTypes()[i]]++;
	}
	
	// Write name line.
	for(int i = 0; i < numAtomTypes(); i++) {
		if(typeCount[i] > 0) stream << atomTypeName(i) << " ";
	}
	stream << endl;

	// Write count line.
	for(int i = 0; i < numAtomTypes(); i++) {
		if(typeCount[i] > 0) stream << typeCount[i] << " ";
	}
	stream << endl;
    stream << "Cartesian" << endl;

	// Write atomic positions.
	for(int type = 0; type < numAtomTypes(); type++) {
		for(int j = 0; j < natoms; j++) {
			if(atomTypes()[j] == type) {
				stream << setw(20) << (atomPositions()[j].x() - simulationCellOrigin().x()) 
					   << setw(20) << (atomPositions()[j].y() - simulationCellOrigin().y()) 
					   << setw(20) << (atomPositions()[j].z() - simulationCellOrigin().z()) << endl;
			}
		}	
	}
}

/******************************************************************************
 * Exports the structure to a LAMMPS dump file.
 ******************************************************************************/
void AtomicStructure::writeToDumpFile(const std::string& filename, bool includeGhostAtoms) const
{
	ofstream stream(filename.c_str());
	if(!stream.is_open())
		throw runtime_error("Could not open LAMMPS dump file for writing.");

	stream << "ITEM: TIMESTEP" << endl;
	stream << "0" << endl;
	if(simulationCell()(0,1) == 0.0 && simulationCell()(0,2) == 0.0 && simulationCell()(1,2) == 0.0) {
		stream << "ITEM: BOX BOUNDS";
		for(int k = 0; k < 3; k++) {
			if(hasPBC(k)) stream << " pp";
			else stream << " ss";
		}
		stream << endl;
		stream << simulationCellOrigin().x() << " " << (simulationCellOrigin().x() + simulationCell()(0,0)) << endl;
		stream << simulationCellOrigin().y() << " " << (simulationCellOrigin().y() + simulationCell()(1,1)) << endl;
		stream << simulationCellOrigin().z() << " " << (simulationCellOrigin().z() + simulationCell()(2,2)) << endl;
	}
	else {
		stream << "ITEM: BOX BOUNDS xy xz yz";
		for(int k = 0; k < 3; k++) {
			if(hasPBC(k)) stream << " pp";
			else stream << " ss";
		}
		stream << endl;
		FloatType xlo = simulationCellOrigin().x();
		FloatType ylo = simulationCellOrigin().y();
		FloatType zlo = simulationCellOrigin().z();
		FloatType xhi = simulationCell().column(0).x() + xlo;
		FloatType yhi = simulationCell().column(1).y() + ylo;
		FloatType zhi = simulationCell().column(2).z() + zlo;
		FloatType xy = simulationCell().column(1).x();
		FloatType xz = simulationCell().column(2).x();
		FloatType yz = simulationCell().column(2).y();
		xlo = min(xlo, xlo+xy);
		xlo = min(xlo, xlo+xz);
		ylo = min(ylo, ylo+yz);
		xhi = max(xhi, xhi+xy);
		xhi = max(xhi, xhi+xz);
		yhi = max(yhi, yhi+yz);
		stream << xlo << " " << xhi << " " << xy << endl;
		stream << ylo << " " << yhi << " " << xz << endl;
		stream << zlo << " " << zhi << " " << yz << endl;
	}

	int natoms = includeGhostAtoms ? numAtoms() : numLocalAtoms();
	stream << "ITEM: NUMBER OF ATOMS" << endl;
	stream << natoms << endl;
	stream << "ITEM: ATOMS id type x y z";
	stream << endl;
	for(int i = 0; i < natoms; i++) {
		stream << (i+1) << " " << atomTypes()[i] << " ";
		stream << atomPositions()[i].x() << " " << atomPositions()[i].y() << " " << atomPositions()[i].z() << endl;
	}
}


/******************************************************************************
 * Loads a LAMMPS dump file into the user structure.
 ******************************************************************************/
void AtomicStructure::loadLAMMPSDumpFile(const std::string& filepath)
{
	// Open input file for reading.
	ifstream stream(filepath.c_str());
	if(!stream.is_open())
		throw runtime_error("Could not open LAMMPS dump file.");

	MsgLogger(debug) << "Parsing LAMMPS dump file:" << endl;

	bool readNumberOfAtoms = false, readBoxBounds = false, readAtoms = false;

	while(!stream.eof()) {
		string line;
		getline(stream, line);

		do {
			if(starts_with(line, "ITEM: TIMESTEP")) {
				getline(stream, line);	// Ignore timestep number
				break;
			}
			else if(starts_with(line, "ITEM: NUMBER OF ATOMS")) {
				// Parse number of atoms.
				int numAtoms;
				stream >> numAtoms;
				if(!stream || numAtoms < 0 || numAtoms > 1e9)
					throw runtime_error("LAMMPS dump file parsing error. Invalid number of atoms.");
				MsgLogger(debug) << "  numatoms " << numAtoms << endl;
				setAtomCount(numAtoms);
				readNumberOfAtoms = true;
				break;
			}
			else if(starts_with(line, "ITEM: BOX BOUNDS")) {

				size_t pbcInfoStart = 16;
				if(starts_with(line, "ITEM: BOX BOUNDS xy xz yz"))
					pbcInfoStart += 9;

				// Parse PBC flags
				std::array<bool,3> pbc;
				pbc.fill(true); // Assume periodic boundary conditions by default.
				istringstream linestream(line.substr(pbcInfoStart));
				for(int k = 0; k < 3; k++) {
					string pbcflag;
					linestream >> pbcflag;
					if(linestream && pbcflag != "pp")
						pbc[k] = false;
				}
				MsgLogger(debug) << "  pbc " << pbc[0] << " " << pbc[1] << " " << pbc[2] << endl;

				// Parse simulation box dimensions.
				double tiltFactors[3] = {0,0,0};
				double simBoxMin[3];
				double simBoxMax[3];
				for(int k = 0; k < 3; k++) {
					getline(stream, line);
					istringstream linestream(line);
					linestream >> simBoxMin[k] >> simBoxMax[k] >> tiltFactors[k];
				}
				if(!stream)
					throw runtime_error("LAMMPS dump file parsing error. Invalid box size.");

				// LAMMPS only stores the outer bounding box of the simulation cell in the dump file.
				// We have to determine the size of the actual triclinic cell.
				simBoxMin[0] -= min(min(min(tiltFactors[0], tiltFactors[1]), tiltFactors[0]+tiltFactors[1]), (FloatType)0.0);
				simBoxMax[0] -= max(max(max(tiltFactors[0], tiltFactors[1]), tiltFactors[0]+tiltFactors[1]), (FloatType)0.0);
				simBoxMin[1] -= min(tiltFactors[2], 0.0);
				simBoxMax[1] -= max(tiltFactors[2], 0.0);
				setupSimulationCell(Matrix3(
						Vector3(simBoxMax[0] - simBoxMin[0], 0, 0),
						Vector3(tiltFactors[0], simBoxMax[1] - simBoxMin[1], 0),
						Vector3(tiltFactors[1], tiltFactors[2], simBoxMax[2] - simBoxMin[2])),
						Point3(simBoxMin[0], simBoxMin[1], simBoxMin[2]),
						pbc);
				MsgLogger(debug) << "  cell " << endl << simulationCell();
				readBoxBounds = true;
				break;
			}
			else if(starts_with(line, "ITEM: ATOMS")) {

				// Parse column names.
				int columnPosX = -1;
				int columnPosY = -1;
				int columnPosZ = -1;
				int columnForceX = -1;
				int columnForceY = -1;
				int columnForceZ = -1;
				int columnDisplX = -1;
				int columnDisplY = -1;
				int columnDisplZ = -1;
				int columnId = -1;
				int columnType = -1;
				bool reducedCoordinates = false;
				string subLine = line.substr(11);
				tokenizer<> tokens(subLine);
				int numberOfColumns = 0;
				for(tokenizer<>::iterator token = tokens.begin(); token != tokens.end(); ++token, numberOfColumns++) {
					string columnName = *token;
					if     (columnName == "x" || columnName == "xu") columnPosX = numberOfColumns;
					else if(columnName == "y" || columnName == "yu") columnPosY = numberOfColumns;
					else if(columnName == "z" || columnName == "zu") columnPosZ = numberOfColumns;
					else if(columnName == "xs") { columnPosX = numberOfColumns; reducedCoordinates = true; }
					else if(columnName == "ys") { columnPosY = numberOfColumns; reducedCoordinates = true; }
					else if(columnName == "zs") { columnPosZ = numberOfColumns; reducedCoordinates = true; }
					else if(columnName == "fx") columnForceX = numberOfColumns;
					else if(columnName == "fy") columnForceY = numberOfColumns;
					else if(columnName == "fz") columnForceZ = numberOfColumns;
					else if(columnName == "dx") columnDisplX = numberOfColumns;
					else if(columnName == "dy") columnDisplY = numberOfColumns;
					else if(columnName == "dz") columnDisplZ = numberOfColumns;
					else if(columnName == "id") columnId = numberOfColumns;
					else if(columnName == "type") columnType = numberOfColumns;
				}
				if(numberOfColumns == 0) throw runtime_error("LAMMPS dump file parsing error. Input file does not contain column identifiers. File format is too old.");
				if(columnPosX == -1) throw runtime_error("LAMMPS dump file parsing error. Input file does not contain X coordinates column.");
				if(columnPosY == -1) throw runtime_error("LAMMPS dump file parsing error. Input file does not contain Y coordinates column.");
				if(columnPosZ == -1) throw runtime_error("LAMMPS dump file parsing error. Input file does not contain Z coordinates column.");

				// Parse one atom per line.
				for(int i = 0; i < numLocalAtoms(); i++) {
					Point3 pos = Point3::Origin();
					Point3 displ = Point3::Origin();
					Vector3 force = Vector3::Zero();
					int id = i+1;
					int type = 0;

					getline(stream, line);
					istringstream linestream(line);
					for(int columnIndex = 0; columnIndex < numberOfColumns; columnIndex++) {
						if     (columnPosX   == columnIndex) linestream >> pos.x();
						else if(columnPosY   == columnIndex) linestream >> pos.y();
						else if(columnPosZ   == columnIndex) linestream >> pos.z();
						else if(columnDisplX == columnIndex) linestream >> displ.x();
						else if(columnDisplY == columnIndex) linestream >> displ.y();
						else if(columnDisplZ == columnIndex) linestream >> displ.z();
						else if(columnForceX == columnIndex) linestream >> force.x();
						else if(columnForceY == columnIndex) linestream >> force.y();
						else if(columnForceZ == columnIndex) linestream >> force.z();
						else if(columnIndex == columnId) linestream >> id;
						else if(columnIndex == columnType) {
							linestream >> type;
							type--;
						}
						else {
							string token;
							linestream >> token;	// Skip column.
						}
					}
					if(!stream || !linestream)
						throw runtime_error("LAMMPS dump file parsing error. Invalid atom line.");

					// Rescale reduced atom coordinates.
					if(reducedCoordinates)
						pos = reducedToAbsolute(pos);

					// Check atom index.
					--id;
					if(id < 0 || id >= numLocalAtoms())
						throw runtime_error("LAMMPS dump file parsing error. Atom index out of range.");

					// Remap atom type to internal numbering.
					map<int, int>::const_iterator remap_iter = _atomTypeRemap.find(type);
					if(remap_iter != _atomTypeRemap.end())
						type = remap_iter->second;
					if(type < 0 || type >= job()->numAtomTypes())
						throw runtime_error("LAMMPS dump file parsing error. Atom type out of range.");

					atomPositions()[id] = pos;
					atomDisplacements()[id] = displ;
					atomTypes()[id] = type;
					atomForcesProperty().setTargetValue(id, force);

					MsgLogger(debug) << "  atom: "
							 << "  id " << id
							 << "  type " << type
							 << "  pos " << pos
							 << "  displ " << displ
							 << "  force " << force
							 << endl;
				}
				setDirty(ATOM_POSITIONS);
				readAtoms = true;
				return;	// We are done.
			}
			else if(starts_with(line, "ITEM:")) {
				// Skip lines up to next ITEM.
				while(!stream.eof()) {
					getline(stream, line);
					if(starts_with(line, "ITEM:")) break;
				}
			}
			else if(line.empty() && stream) {
				// Skip empty lines
				break;
			}
			else {
				throw runtime_error("LAMMPS dump file parsing error. Invalid line in dump file.");
			}
		}
		while(!stream.eof());
	}
	if(!readNumberOfAtoms) throw runtime_error("LAMMPS dump file parsing error. Number of atoms could not be read.");
	if(!readBoxBounds) throw runtime_error("LAMMPS dump file parsing error. Box bounds could not be read.");
	if(!readAtoms) throw runtime_error("LAMMPS dump file parsing error. Atoms could not be read.");
}

/******************************************************************************
 * Loads a POSCAR formatted file into the user structure. The format is
 * slightly more flexible to allow forces and energies to be provided.
 ******************************************************************************/
void AtomicStructure::loadPOSCARFile(const std::string& filepath)
{
	// Open input file for reading.
	ifstream stream(filepath.c_str());
	if(!stream.is_open())
		throw runtime_error("Could not open POSCAR file.");

	std::string line;

	// Skip comment line.
	getline(stream, line);
	MsgLogger(debug) << "Parsing POSCAR file:" << endl;
	MsgLogger(debug) << "  comment " << line << endl;

	// Lattice scaling factor
	getline(stream, line);
	double alat;
	istringstream(line) >> alat;
	MsgLogger(debug) << "  alat " << alat << endl;
	if(alat <= 0)
		throw runtime_error("Invalid lattice constant in POSCAR file.");

	// Cell metric.
	Matrix3 simCell;
	for(size_t i = 0; i < 3; i++) {
		getline(stream, line);
		istringstream(line) >> simCell(0,i) >> simCell(1,i) >> simCell(2,i);
	}
	simCell = simCell * alat;
	if(simCell.determinant() <= 0)
		throw runtime_error("Invalid cell metric in POSCAR file.");
	setupSimulationCell(simCell);
	MsgLogger(debug) << "  cell " << endl << simCell;

	// Parse atom type names.
	vector<int> numberOfAtomsPerType;
	vector<std::string> atomNames;
	for(int i = 0; i < 2; i++) {
		getline(stream, line);
		tokenizer<> tokens(line);
		// Try to convert string tokens to integers.
		try {
			numberOfAtomsPerType.clear();
			for(tokenizer<>::iterator str=tokens.begin() ; str!=tokens.end() ; ++str)
				numberOfAtomsPerType.push_back (boost::lexical_cast<int>(*str));
			break;
		}
		catch(const boost::bad_lexical_cast&) {
		  // If the casting to integer fails, then the current line contains the element names.
		  atomNames.clear();
		  for(tokenizer<>::iterator str=tokens.begin() ; str!=tokens.end() ; ++str)
		    atomNames.push_back(*str);
		}
	}

	if(numberOfAtomsPerType.empty())
		throw runtime_error("Invalid atom type counts in POSCAR file.");

	for(std::vector<int>::iterator iter = numberOfAtomsPerType.begin(); iter != numberOfAtomsPerType.end(); ++iter)
		MsgLogger(debug) << "Number of atoms per type " << *iter << endl;

	// Compute cumulative atom type counts.
	vector<int> cumulativeAtomCounts;
	boost::partial_sum(numberOfAtomsPerType, back_inserter(cumulativeAtomCounts));

	setAtomCount(cumulativeAtomCounts.back());

	// Read format of atom coordinates.
	getline(stream, line);
	trim_left(line);
	to_upper(line);
	bool reducedCoordinates;
	if(starts_with(line, "D"))
		reducedCoordinates = true;
	else if(starts_with(line, "C") || starts_with(line, "K"))
		reducedCoordinates = false;
	else
		throw runtime_error("POSCAR parser can only handle direct coordinates.");
	MsgLogger(debug) << "  reducedCoordinates " << reducedCoordinates << endl;

	// Parse atomic coordinates (and optionally force vectors).
	bool containsForceVectors = false;
	for(int i = 0; i < numLocalAtoms(); i++) {

		// Read xyz coordinates.
		Point3 pos;
		getline(stream, line);
		istringstream linestream(line);
		linestream >> pos.x() >> pos.y() >> pos.z();

		// Read force vector components.
		Vector3 force;
		linestream >> force.x() >> force.y() >> force.z();
		if(linestream)
			containsForceVectors = true;
		else if(containsForceVectors)
			throw runtime_error("POSCAR contains invalid force vector for atom.");

        int atomType = upper_bound(cumulativeAtomCounts, i) - cumulativeAtomCounts.begin();
        if (_atomTypeRemap.empty() && !atomNames.empty()){
            // No remap command given. Now the atom types are set according to the
            // atom names read in earlier.
            int sum = 0;
            for (int j = 0; j < numberOfAtomsPerType.size(); j++){
                sum += numberOfAtomsPerType[j];
                if (sum > i){
                    atomType = job()->atomTypeIndex(atomNames[j]);
                    break;
                }
            }
        }
        else{
            // Remap atom type to internal numbering.
            map<int, int>::const_iterator remap_iter = _atomTypeRemap.find(atomType);
            if(remap_iter != _atomTypeRemap.end())
                atomType = remap_iter->second;
            if(atomType < 0 || atomType >= job()->numAtomTypes())
                throw runtime_error("POSCAR file parsing error. Atom type is out of range.");
        }

		// Rescale reduced atom coordinates.
		if(reducedCoordinates)
			pos = reducedToAbsolute(pos);

		// Store values in appropriate arrays.
		atomPositions()[i] = pos;
		atomTypes()[i] = atomType;
		atomForcesProperty().setTargetValue(i, force);
	}

	MsgLogger(debug) << "  containsForceVectors " << containsForceVectors << endl;

	setDirty(ATOM_POSITIONS);
}

}
