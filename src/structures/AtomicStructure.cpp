///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructure.h"
#include "../potentials/Potential.h"
#include "../minimizers/LBFGSMinimizer.h"

namespace nanoRVE {

using namespace std;


/******************************************************************************
 * Setup the cell geometry.
 ******************************************************************************/
void AtomicStructure::setupSimulationCell(const Matrix3& cellVectors, const Point3& cellOrigin, const std::array<bool,3>& pbc)
{
	if(cellVectors.determinant() == 0.0)
		throw runtime_error("Simulation cell is degenerate.");

	_simulationCell = cellVectors;
	_simulationCellOrigin = cellOrigin;
	_reciprocalSimulationCell = cellVectors.inverse();

	if(pbc != _pbc)
		setDirty(NEIGHBOR_LISTS);

	_pbc = pbc;

	// Need to update ghost atoms and possibly neighbor lists.
	setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Applies an affine transformation to the cell and the atoms.
 ******************************************************************************/
void AtomicStructure::deformSimulationCell(const Matrix3& deformation)
{
	// Modify cell shape matrix.
	setupSimulationCell(deformation * simulationCell(), simulationCellOrigin(), _pbc);

	// Transform atoms by the given matrix.
	for(int i = 0; i < numLocalAtoms(); i++)
		atomPositions()[i] = simulationCellOrigin() + deformation * (atomPositions()[i] - simulationCellOrigin());

	// Atomic positions have changed.
	setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Determines the periodic image of the simulation cell in which the given
 * point is located.
 ******************************************************************************/
Vector3I AtomicStructure::periodicImage(const Point3& p) const
{
	Vector3 rp = reciprocalSimulationCell() * (p - simulationCellOrigin());
	return Vector3I(hasPBC(0) ? (int)floor(rp.x()) : 0,
					hasPBC(1) ? (int)floor(rp.y()) : 0,
					hasPBC(2) ? (int)floor(rp.z()) : 0);
}

/******************************************************************************
 * Wraps a point to be inside the simulation cell.
 ******************************************************************************/
Point3 AtomicStructure::wrapPoint(Point3 p) const
{
	// Transform point to reduced cell coordinates.
	Vector3 rp = reciprocalSimulationCell() * (p - simulationCellOrigin());
	for(size_t dim = 0; dim < 3; dim++) {
		if(hasPBC(dim)) {
			if(FloatType s = floor(rp[dim]))
				p -= s * simulationCell().column(dim);
		}
	}
	return p;
}

/******************************************************************************
 * Wraps a point in reduced coordinates to be inside the simulation cell.
 ******************************************************************************/
Point3 AtomicStructure::wrapReducedPoint(Point3 p) const
{
	for(size_t dim = 0; dim < 3; dim++) {
		if(hasPBC(dim)) {
			p[dim] -= floor(p[dim]);
			assert(p[dim] >= 0);
			assert(p[dim] < 1);
		}
	}
	return p;
}

/******************************************************************************
 * Deletes all existing atoms and resizes the atoms array.
 ******************************************************************************/
void AtomicStructure::setAtomCount(int numLocalAtoms)
{
	if(numLocalAtoms == _numLocalAtoms)
		return;

	_numLocalAtoms = numLocalAtoms;
	_numAtoms = numLocalAtoms;
	_numGhostAtoms = 0;
	_atomPositions.resize(_numLocalAtoms, Point3::Origin());
	_atomDisplacements.resize(_numLocalAtoms, Point3::Origin());
	_atomTypes.resize(_numLocalAtoms, 0);
	_atomTags.resize(_numLocalAtoms, 0);
	_atomForces.resize(_numLocalAtoms);
	_atomInitialPositions.clear();
	_lastNeighborUpdatePositions.clear();

	setDirty(NEIGHBOR_LISTS);
	setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Updates the structure, creates ghost atoms and builds neighbor lists.
 ******************************************************************************/
void AtomicStructure::updateStructure()
{
	// Determine whether neighbor lists need to be rebuilt.
	bool rebuildNeighborLists = false;
	if(isDirty(NEIGHBOR_LISTS) || _lastNeighborUpdatePositions.size() != numLocalAtoms()) {
		// Need to rebuild every time the number of atoms has changed.
		rebuildNeighborLists = true;
		_lastNeighborUpdatePositions.clear();
		_skinThickness = 0;

		MsgLogger(maximum) << "Neighbor lists will be built for the first time." << endl;

	}
	else if(isDirty(ATOM_POSITIONS)) {
		// Check if any atom has moved by a distance larger than half of the skin thickness.
		double threshold = _skinThickness / 2;

		// Reduce threshold by deformation of the simulation cell.
		Matrix3 simCellDelta = _simulationCell - _lastSimulationCell;
		double maxCellDeformation = 0;
		for(int ix = 0; ix <= 1; ix++) {
			for(int iy = 0; iy <= 1; iy++) {
				for(int iz = 0; iz <= 1; iz++) {
					Vector3 delta = simCellDelta * Vector3(ix * (_ghostAtomImages[0]+1), iy * (_ghostAtomImages[1]+1), iz * (_ghostAtomImages[1]+1));
					maxCellDeformation = std::max(maxCellDeformation, delta.squaredLength());
				}
			}
		}
		maxCellDeformation = sqrt(maxCellDeformation);
		threshold -= 0.5 * maxCellDeformation;

		if(threshold <= 0.0) {
			// Simulation cell has already changed so much, internal motion of atoms doesn't matter.
			rebuildNeighborLists = true;
			MsgLogger(maximum) << "Neighbor lists need to be rebuilt because of cell deformation (skin=" << _skinThickness << ", cell_def=" << maxCellDeformation << " threshold=" << threshold << ")." << endl;
		}
		else {

			// Check atomic displacements against threshold.
			double thresholdSquared = threshold * threshold;
			for(size_t i = 0; i < _lastNeighborUpdatePositions.size(); i++) {
				Vector3 delta = _lastNeighborUpdatePositions[i] - _atomPositions[i];
				if(delta.squaredLength() > thresholdSquared) {
					rebuildNeighborLists = true;
					MsgLogger(maximum) << "Neighbor lists need to be rebuilt because of atomic displacement (skin=" << _skinThickness << ", threshold=" << threshold << ")." << endl;
					break;
				}
			}
		}
	}

	// Assign atom tags if not done yet.
	if(_atomTags.size() < numLocalAtoms()) {
		_atomTags.resize(numLocalAtoms());
		for(int i = 0; i < numLocalAtoms(); i++) {
			_atomTags[i] = i + 1;

			// Check atom type.
			if(atomTypes()[i] < 0 || atomTypes()[i] >= numAtomTypes())
				throw runtime_error("At least one atom has an out-of-range atom type.");
		}
	}

    // Store a copy of all atomic positions when this function is called for the first time,
	// such that we can revert back to them when necessary.
	if(_atomInitialPositions.empty()) {
        MsgLogger(debug) << "Saving initial atom positions." << endl;
		_atomInitialPositions = _atomPositions;
		assert(_atomPositions.size() == numLocalAtoms());
	}

	if(rebuildNeighborLists) {

		// Determine a reasonable finite neighbor skin thickness the first time the atoms have moved or the cell has been deformed.
		if(_skinThickness <= 0 && _lastNeighborUpdatePositions.empty() == false) {
			// Skin thickness automatically scales with the average interatomic distance in the structure.
			_skinThickness = 0.8 * pow(std::abs(simulationCell().determinant()) / numLocalAtoms(), 1.0/3.0);
		}

		// Make a copy of the current atom positions so that we can detect when the ghost atoms and neighbor lists need
		// to be regenerated again.
		_lastNeighborUpdatePositions.resize(numLocalAtoms());
		std::copy_n(_atomPositions.cbegin(), numLocalAtoms(), _lastNeighborUpdatePositions.begin());
		_lastSimulationCell = _simulationCell;

		// The thickness of of the ghost atom layer is determined by the range of the potential
		// and an extra "skin" thickness, which saves us from rebuilding the ghost atom layer too often.
		double ghostLayerThickness = maximumCutoff() + _skinThickness;

		// Determine how many copies we have to make of the real atoms.
		const int planeVectors[3][2] = { {1,2}, {2,0}, {0,1} };
		FloatType cuts[3][2];
		Vector3 cellNormals[3];
		for(size_t dim = 0; dim < 3; dim++) {
			Vector3 e1 = simulationCell().column(planeVectors[dim][0]);
			Vector3 e2 = simulationCell().column(planeVectors[dim][1]);
			cellNormals[dim] = e1.cross(e2).normalized();
			cuts[dim][0] = cellNormals[dim].dot(reducedToAbsolute(Point3(0,0,0)) - Point3::Origin());
			cuts[dim][1] = cellNormals[dim].dot(reducedToAbsolute(Point3(1,1,1)) - Point3::Origin());

			if(hasPBC(dim)) {
				_ghostAtomImages[dim] = (int)ceil(ghostLayerThickness / std::abs(simulationCell().column(dim).dot(cellNormals[dim])));
				cuts[dim][0] -= ghostLayerThickness;
				cuts[dim][1] += ghostLayerThickness;
				if(_ghostAtomImages[dim] > 100)
					throw runtime_error("Simulation cell became too small.");
				if(_ghostAtomImages[dim] <= 0)
					throw runtime_error("Simulation cell dimension is invalid.");
			}
			else {
				_ghostAtomImages[dim] = 0;
				cuts[dim][0] -= ghostLayerThickness;
				cuts[dim][1] += ghostLayerThickness;
			}
		}
		_numGhostAtoms = 0;
		_numAtoms = numLocalAtoms();
		_atomPositions.resize(numLocalAtoms());
		_atomDisplacements.resize(numLocalAtoms());
		_atomTypes.resize(numLocalAtoms());
		_atomTags.resize(numLocalAtoms());

		// Create ghost atoms.
		_forwardMapping.clear();
		_reverseMapping.clear();
		_ghostAtomPBCImages.clear();
		int ghostIndex = numLocalAtoms();
		for(int ix = -_ghostAtomImages[0]; ix <= _ghostAtomImages[0]; ix++) {
			for(int iy = -_ghostAtomImages[1]; iy <= _ghostAtomImages[1]; iy++) {
				for(int iz = -_ghostAtomImages[2]; iz <= _ghostAtomImages[2]; iz++) {
					if(ix == 0 && iy == 0 && iz == 0) continue;
					Vector3 pbcImage = Vector3(ix, iy, iz);
					Vector3 shift = simulationCell() * pbcImage;
					for(int i = 0; i < numLocalAtoms(); i++) {
						Point3 pimage = atomPositions()[i] + shift;

						bool isClipped = false;
						for(size_t dim = 0; dim < 3; dim++) {
							FloatType d = cellNormals[dim].dot(pimage - Point3::Origin());
							if(d < cuts[dim][0] || d > cuts[dim][1]) {
								isClipped = true;
								break;
							}
						}
						if(!isClipped) {
							atomPositions().push_back(pimage);
							atomDisplacements().push_back(atomDisplacements()[i]);
							atomTypes().push_back(atomTypes()[i]);
							atomTags().push_back(atomTags()[i]);
							_reverseMapping.push_back(i);
							_forwardMapping.push_back(std::make_pair(i, ghostIndex));
							_ghostAtomPBCImages.push_back(pbcImage);
							_numGhostAtoms++;
							_numAtoms++;
							ghostIndex++;
						}
					}
				}
			}
		}
		assert(ghostIndex == numAtoms());
		assert(_atomPositions.size() == _numAtoms);
		assert(_atomDisplacements.size() == _numAtoms);
		assert(_atomTypes.size() == _numAtoms);
		assert(_atomTags.size() == _numAtoms);

		// Allocate per-atom data buffer.
		size_t maxPerAtomDataSize = 0;
		for(const auto& pot : potentials())
			maxPerAtomDataSize = max(maxPerAtomDataSize, pot->perAtomDataSize());
		_perAtomPotentialData.resize(maxPerAtomDataSize * numAtoms());

		// Build the neighbor list for each potential.
		for(const auto& pot : potentials()) {
			_neighborLists[pot.get()].build(*this, *pot, pot->cutoff() + _skinThickness);
		}
	}
	else if(isDirty(ATOM_POSITIONS)) {

		// Update positions of ghost atoms when local atoms have moved.
		MsgLogger(debug) << "Updating ghost atom positions." << endl;
		auto reverseMap = reverseMapping().cbegin();
		auto pbcImage = _ghostAtomPBCImages.cbegin();
		for(auto pos = atomPositions().begin() + numLocalAtoms(); pos != atomPositions().end(); ++pos, ++reverseMap, ++pbcImage) {
			*pos = atomPositions()[*reverseMap] + simulationCell() * (*pbcImage);
		}

		// Update interatomic vectors stored in the neighbor lists.
		for(const auto& pot : potentials()) {
			_neighborLists[pot.get()].update(*this);
		}
	}

	// Reset dirty flags now that we have updated everything.
	_dirtyFlags = 0;
}

/**
  @brief Computes the total energy and optionally the forces of this structure.
  @details By default, performs relaxation of atomic positions if enabled by the user.
  @param computeForces if True the forces will be evaluated as well
  @param isFitting determines whether this DOF should be relaxed (usually the desired behavior depends on the program phase)
  @param suppressRelaxation if True structural relaxation will be suppressed
 */
double AtomicStructure::computeEnergy(bool computeForces, bool isFitting, bool suppressRelaxation)
{
    // Make sure that everything is in place (atoms, neighbor lists etc.).
    updateStructure();

    int numRelaxDOF = _atomCoordinatesDOF.numScalars();

	if(!suppressRelaxation && _atomCoordinatesDOF.relax() && numRelaxDOF > 0) {

        // determine if the atomic positions should be reset
        if (_atomCoordinatesDOF.resetBeforeRelax() && job()->fitMinimizer() && _lastUpdate < job()->fitMinimizer()->itercount()){
            _atomCoordinatesDOF.reset();
            _lastUpdate = job()->fitMinimizer()->itercount();
            MsgLogger(maximum) << "Resetting atom positions of structure '" << id() << "'." << endl;
        }

		// The internal atomic degrees of freedom must be relaxed first.
		MsgLogger(debug) << "Relaxing atomic d.o.f. of structure " << id()
                         << " (#dof=" << numRelaxDOF << "):" << endl;

		vector<double> x0(numRelaxDOF);
		double* x0_iter = x0.data();
		_atomCoordinatesDOF.exportValue(x0_iter);
		assert(x0_iter == x0.data() + x0.size());

		// Define objective function.
		auto function = [this, isFitting](const std::vector<double>& x) -> double {
			const double* src_iter = x.data();
			_atomCoordinatesDOF.importValue(src_iter);
			assert(src_iter == x.data() + x.size());
			return computeEnergy(false, isFitting, true);
		};

		// Define gradient function.
		auto gradient = [this, isFitting](const std::vector<double>& x, std::vector<double>& g) -> double {
			// Use analytic gradient (forces) computed by the potential.
			const double* src_iter = x.data();
			_atomCoordinatesDOF.importValue(src_iter);
			assert(src_iter == x.data() + x.size());
			double e = computeEnergy(true, isFitting, true);
			auto force_iter = atomForces().begin();
			for(auto g_iter = g.begin(); g_iter != g.end(); ) {
				*g_iter++ = -force_iter->x();
				*g_iter++ = -force_iter->y();
				*g_iter++ = -force_iter->z();
				++force_iter;
			}
			++force_iter;
			assert(force_iter == atomForces().end());
			return e;
		};

		// Initialize minimizer.
		LBFGSMinimizer minimizer(job());
		minimizer.prepare(std::move(x0), function, gradient);

		Minimizer::MinimizerResult result;
		while((result = minimizer.iterate()) == Minimizer::MINIMIZATION_CONTINUE) {
			MsgLogger(debug) << "        force_rlx_iter=" << minimizer.itercount()
							   << " E=" << minimizer.value()
							   << " fnorm=" << minimizer.gradientNorm2() << endl;
		}
		if(result == Minimizer::MINIMIZATION_ERROR)
			throw runtime_error(str(format("The minimizer reported an error when relaxing structure '%1%'.") % id()));

		return computeEnergy(computeForces, isFitting, true);
	}
	else {

		_totalEnergy = 0.0;

		assert(_neighborLists.size() == job()->potentials().size());
		if(computeForces) {

			// Reset forces.
			fill(_atomForcesProperty.values(), Vector3::Zero());

			// Reset virial.
			_virial.fill(0);

			// Calculate energy and forces.
			for(Potential* pot : job()->potentials()) {
				_totalEnergy += pot->computeEnergyAndForces(*this, neighborList(pot));
			}

#ifdef _DEBUG
			for(int i = 0; i < numLocalAtoms(); i++) {
				assert(std::isnan(atomForces()[i].x()) == false);
				assert(std::isnan(atomForces()[i].y()) == false);
				assert(std::isnan(atomForces()[i].z()) == false);
			}
#endif

#if 0
			// Compute virial.
			vector<Vector3>::const_iterator f = atomForces().begin();
			vector<Point3>::const_iterator p = atomPositions().begin();
			for(int i = 0; i < numLocalAtoms(); i++, ++f, ++p) {
				_virial[0] += f->x() * p->x();
				_virial[1] += f->y() * p->y();
				_virial[2] += f->z() * p->z();
				_virial[3] += f->z() * p->y();
				_virial[4] += f->z() * p->x();
				_virial[5] += f->y() * p->x();
			}
#endif

			// Compute pressure tensor (in bar units).
			double invVolume = 1.0 / fabs(simulationCell().determinant()) * 1.6021765e6;
			for(int i = 0; i < 6; i++)
				_pressureTensorProperty[i] = _virial[i] * invVolume;
			_pressureProperty = (_virial[0] + _virial[1] + _virial[2]) * invVolume / 3.0;
		}
		else {
			// Calculate energy only.
			for(Potential* pot : job()->potentials()) {
				_totalEnergy += pot->computeEnergy(*this, neighborList(pot));
			}
		}

		MsgLogger(debug) << "Computed energy of structure " << id()
						 << "  E=" << _totalEnergy << "  local atoms: " << numLocalAtoms()
						 << "  total atoms: " << numAtoms() << endl;

		// if(!std::isfinite(_totalEnergy))
			// throw runtime_error(str(format("The total energy calculated for structure '%1%' was '%2%'.") % id() % _totalEnergy));

		return _totalEnergy;
	}
}

}
