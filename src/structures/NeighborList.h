///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski
//
//  This file is part of nanoRVE.
//
//  nanoRVE is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  nanoRVE is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../nanoRVE.h"

namespace nanoRVE {

class AtomicStructure;
class Potential;

struct NeighborListEntry
{
	Vector3 delta;				// delta = x[j] - x[i]
	double r;					// r = |delta|
	int index;					// Index of second atom of the neighbor bond (this may be a local or a ghost atom).
	int localIndex;				// Index of second local atom (this is always the corresponding local atom).

	// Pointer to additional memory for the potential routines to store pair-wise information.
	void* bondDataPtr;

	template<typename T>
	T& bondData() {
		assert(bondDataPtr != nullptr);
		return *static_cast<T*>(bondDataPtr);
	}

	template<typename T>
	const T& bondData() const {
		assert(bondDataPtr != nullptr);
		return *static_cast<const T*>(bondDataPtr);
	}
};

/**
 * Stores the half and full neighbor lists for a
 * structure/potential combination.
 */
class NeighborList
{
public:

	/// Constructor.
	NeighborList() : _numAtoms(0) {}

	/// Returns the number of atoms included in this neighbor list.
	int numAtoms() const { return _numAtoms; }

	/// Returns the index of the i-th atom in this neighbor list.
	int atomIndex(int i) const { return _ilist[i]; }

	/// Returns the number of neighbors of a real atom (half neighbor list).
	int numNeighborsHalf(int atomIndex) const { return _numNeighborsHalf[atomIndex]; }

	/// Returns the number of neighbors of a real atom (full neighbor list).
	int numNeighborsFull(int atomIndex) const { return _numNeighborsFull[atomIndex]; }

	/// Returns a pointer to the first entry in the neighbor list of the given real atom.
	NeighborListEntry* neighborList(int atomIndex) {
		assert(atomIndex >= 0 && atomIndex < _firstNeighbor.size());
		return &_neighborList[_firstNeighbor[atomIndex]];
	}

	/// Returns a const-pointer to the first entry in the neighbor list of the given real atom.
	const NeighborListEntry* neighborList(int atomIndex) const {
		assert(atomIndex >= 0 && atomIndex < _firstNeighbor.size());
		return &_neighborList[_firstNeighbor[atomIndex]];
	}

	/// Generates the neighbor list.
	void build(const AtomicStructure& structure, const Potential& potential, double cutoffRange);

	/// Updates the stored list of interatomic vectors.
	/// This is called when the atoms of the structure have (slightly) moved or if the simulation cell
	/// geometry has (slightly) changed.
	void update(const AtomicStructure& structure);

private:

	/// The number of atoms included in this neighbor list.
	int _numAtoms;
	/// Stores the list of atoms, which are included in the neighbor list.
	std::vector<int> _ilist;
	/// For the half neighbor list, stores the number of neighbors for each real atom.
	std::vector<int> _numNeighborsHalf;
	/// For the full neighbor list, stores the number of neighbors for each real atom.
	std::vector<int> _numNeighborsFull;
	/// Stores the start index into the global array where the neighbor list starts for each real atom.
	std::vector<int> _firstNeighbor;
	/// Stores the neighbor lists.
	std::vector<NeighborListEntry> _neighborList;
	/// Memory buffer that stores temporary per-bond data used by the potential routines.
	std::vector<unsigned char> _perBondPotentialData;
};

} // End of namespace
